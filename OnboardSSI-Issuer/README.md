# Quadible Issuer API

## Setup for local development

Requirements:
- You have received Verity application endpoint, Domain DID and REST API key from Evernym
- You have NodeJs v12 installed
- You have ngrok installed ([https://ngrok.com/](https://ngrok.com/))

To try out sample Issuer app follow these steps:
- In a separate terminal window start ngrok for port 4000 and leave it running :
```sh
ngrok http 4000
```
- Install required NodeJs packages:
```sh
npm install
```
- Create an .env file
```sh
WEBHOOK_URL=https://c713-109-178-146-15.eu.ngrok.io/webhook
VERITY_URL=
DOMAIN_DID=
X_API_KEY=
WEBHOOK_URL=
```
- Start Issuer app
```sh
node Issuer.js
```
Observe messages being exchanged between an Issuer app and a Verity application server on the console output. Scan QR code with a ConnectMe device to establish the connection when required.