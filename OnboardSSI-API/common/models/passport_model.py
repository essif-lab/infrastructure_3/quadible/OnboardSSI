from pydantic import BaseModel

class PassportDBModel(BaseModel):

    user_id: str = (...)
    firstname: str = (...)
    lastname: str = (...)
    passport_id: str = (...)
    photo: bytes = (...)
    birthday: str = (...),
    issue_day: str = (...),
    expiry_day: str = (...),
    birth_place: str = (...),
    authority: str = (...),

    def to_dict_without_photo(self):
        dict = self.to_dict()
        del dict["photo"]
        return dict

    def to_dict(self):
        return {
            "user_id" : self.user_id,
            "passport_id" : self.passport_id,
            "firstname": self.firstname,
            "lastname": self.lastname,
            "photo" : self.photo,
            "birthday" : self.birthday,
            "issue_day" : self.issue_day,
            "expiry_day" : self.expiry_day,
            "birth_place" : self.birth_place,
            "authority" : self.authority,
        }

    # Allow creating from ORM
    class Config:
        orm_mode = True