from pydantic import BaseSettings
from functools import lru_cache
import os 

class Settings(BaseSettings):
    API_V1_STR: str = "/api/v1"
    PROJECT_NAME: str = "SSI-API"
    MASTER_API_KEY = os.getenv('SERVER_API_KEY' , "SSITestingAPIkey!@#")
    DB_NAME = os.getenv('DB_NAME' , "ssidb_tests")
    DB_USER = os.getenv('DB_USER' , "")
    DB_PASS = os.getenv('DB_PASS' , "")
    DB_SERVER_URL = os.getenv('DB_SERVER_URL' , "localhost:27017")
    ISSUER_API_URL = os.getenv('ISSUER_API_URL' , "")

    PASSPORT_COLLLECTION_NAME: str = "passports"
    DB_CONNECTION_STRING : str = None

@lru_cache()
def get_settings():
    print("Loading normal settings")

    settings = Settings()

    # Setup database connection string 
    connection_string = str("mongodb+srv://" 
                                    + settings.DB_USER 
                                    + ":" + settings.DB_PASS 
                                    + "@" + settings.DB_SERVER_URL 
                                    + "/" + settings.DB_NAME 
                                    + "?retryWrites=true&w=majority")

    # This is used for k8s dbs that have no usersname pass 
    if settings.DB_USER == "":
        connection_string = str(f"mongodb://{settings.DB_SERVER_URL}/{settings.DB_NAME}")

    settings.DB_CONNECTION_STRING = connection_string

    return settings