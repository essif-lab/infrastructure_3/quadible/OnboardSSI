from common.config import get_settings
import logging
from motor.motor_asyncio import AsyncIOMotorClient
from common.models.passport_model import PassportDBModel

async def drop_database(db_client: AsyncIOMotorClient):
    
    if get_settings().DB_NAME != "ssidb_tests":
        logging.info("Should not drop database for users other than test DB")
    else:
        # Drop the db  
        await db_client.drop_database(get_settings().DB_NAME)

async def ensure_indexes(db_client: AsyncIOMotorClient):

    db = db_client[get_settings().DB_NAME]

    # Should not create multiple records with the same passport id 
    db[get_settings().PASSPORT_COLLLECTION_NAME].create_index("passport_id", unique=True)
    return

async def insert_passport(db_client: AsyncIOMotorClient, passport_data: PassportDBModel):
    # Insert the passport into the db 
    db = db_client[get_settings().DB_NAME]
    row = await db[get_settings().PASSPORT_COLLLECTION_NAME].insert_one(passport_data.to_dict())
    return row

async def get_passport(db_client: AsyncIOMotorClient, passport_id: str):
    
    # Find the passport from the passport collection 
    db = db_client[get_settings().DB_NAME]
    col = db[get_settings().PASSPORT_COLLLECTION_NAME]
    passport_dict = await col.find_one({"passport_id" : passport_id})
    
    # Handle missing passport 
    if not passport_dict:
        return None

    # Parse the passport model 
    passport_model = PassportDBModel.parse_obj(passport_dict)
    return passport_model

async def delete_passport(db_client: AsyncIOMotorClient, passport_id: str) -> bool: 
    
    # Find the passport from the passport collection 
    db = db_client[get_settings().DB_NAME]
    col = db[get_settings().PASSPORT_COLLLECTION_NAME]
    result = await col.delete_one({"passport_id" : passport_id})
    
    # Check if data were actually deleted 
    deleted = False
    if result.deleted_count > 0:
        deleted = True 

    return deleted