import requests
from common.config import get_settings
import urllib
class IssuerService(object):
    
    @staticmethod
    def initiate_connection(passport_id: str, fullname: str):
        
        fullname = urllib.parse.quote(fullname)
        url = "{base_url}/initiate?name={name}&passport={passport}".format(
            base_url = get_settings().ISSUER_API_URL,
            name = fullname,
            passport = passport_id
        )
        response = requests.request("GET", url)
        return response.text

    @staticmethod
    def get_qr_code(passport_id: str, fullname: str):
        fullname = urllib.parse.quote(fullname)
        url = "{base_url}/getqr?name={name}&passport={passport}".format(
            base_url = get_settings().ISSUER_API_URL,
            name = fullname,
            passport = passport_id
        )
        response = requests.request("GET", url)
        return response