from motor.motor_asyncio import AsyncIOMotorClient

class DataBase:
    client: AsyncIOMotorClient = None

database = DataBase()

async def get_database() -> AsyncIOMotorClient:
    return database.client