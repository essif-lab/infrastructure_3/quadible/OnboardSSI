from motor.motor_asyncio import AsyncIOMotorClient
from common.config import get_settings
from common.db.mongodb import database
import logging
from common.dal.dal_async import async_dal

async def connect_to_mongo():

        settings = get_settings()
        connection_string = settings.DB_CONNECTION_STRING

        database.client = AsyncIOMotorClient(connection_string,maxPoolSize=10,minPoolSize=10)
        print("connect to db:" + connection_string) 

        # In test mode cleanup the db
        if get_settings().DB_NAME == "ssidb_tests":
            await async_dal.drop_database(database.client)

        # Add/ensure index to enrollments 
        await async_dal.ensure_indexes(database.client)

async def close_mongo_connection():
    database.client.close()
    logging.info("Closed db connection")