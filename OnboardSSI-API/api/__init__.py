from fastapi import FastAPI
from api.api import api_router
from common.config import get_settings
from common.db.mongodb_utils import connect_to_mongo, close_mongo_connection

def initialize_app():

    settings = get_settings()

    # Create app 
    app = FastAPI(
        title=settings.PROJECT_NAME, openapi_url=f"{settings.API_V1_STR}/openapi.json"
    )

    # Add router 
    app.include_router(api_router, prefix=settings.API_V1_STR)

    # # Connect to mongo db on API startup
    app.add_event_handler("startup", connect_to_mongo)

    # # # Disconnect from database when shutting down the API
    app.add_event_handler("shutdown", close_mongo_connection)

    return app