import code
from http.client import HTTPResponse
from time import sleep
from fastapi import Depends, APIRouter, Form, UploadFile, File, HTTPException
router = APIRouter()
from motor.motor_asyncio import AsyncIOMotorClient
from common.db.mongodb import get_database
from fastapi.security.api_key import APIKey
from api.core.security import validate_api_key
from typing import Any
from common.dal.dal_async import async_dal
from starlette.status import HTTP_422_UNPROCESSABLE_ENTITY, HTTP_404_NOT_FOUND, HTTP_409_CONFLICT
from fastapi.responses import Response
from common.models.passport_model import PassportDBModel
from common.services.issuer import IssuerService

router = APIRouter()

@router.post("/upload", status_code=201)
async def upload_passport(
    user_id: str = Form(...),
    firstname: str = Form(...),
    lastname: str = Form(...),
    passport_id: str = Form(...),
    birthday: str = Form(...),
    issue_day: str = Form(...),
    expiry_day: str = Form(...),
    birth_place: str = Form(...),
    authority: str = Form(...),
    image: UploadFile = File(...),
    apikey : APIKey = Depends(validate_api_key),
    db_client: AsyncIOMotorClient = Depends(get_database)
) -> Any:

    # Parse the photo from the image parameter
    photo = None
    photo = await image.read()
    if (photo is None):
        raise HTTPException(status_code=HTTP_422_UNPROCESSABLE_ENTITY, detail="Not a valid file was uploaded")

    passport_model = PassportDBModel(
        user_id=user_id, 
        passport_id=passport_id, 
        firstname=firstname, 
        lastname=lastname, 
        photo=photo,
        birthday=birthday,
        issue_day=issue_day,
        expiry_day=expiry_day,
        birth_place=birth_place,
        authority=authority
    )

    try: 
        await async_dal.insert_passport(db_client = db_client, passport_data= passport_model)
    except Exception as ex:
        if ex.code == 11000:
            raise HTTPException(status_code=HTTP_409_CONFLICT, detail="This passport number already exists")
        else:
            raise ex

    return {"Response" : "Passport created"}


@router.get("/passport")
async def get_passport_info(
    passport_id: str,
    apikey : APIKey = Depends(validate_api_key),
    db_client: AsyncIOMotorClient = Depends(get_database)
):
    # Find the passport on the db
    passport = await async_dal.get_passport(db_client = db_client, passport_id=passport_id)
    
    # Handle missing passport 
    if not passport:
        raise HTTPException(status_code=HTTP_404_NOT_FOUND, detail="A passport with this id does not exist")

    # Return the passport without the photo 
    return passport.to_dict_without_photo()

@router.get("/passport_photo")
async def get_passport_photo(
    passport_id: str,
    apikey : APIKey = Depends(validate_api_key),
    db_client: AsyncIOMotorClient = Depends(get_database)
):
    # Find the passport on the db
    passport = await async_dal.get_passport(db_client = db_client, passport_id=passport_id)
    
    # Handle missing passport 
    if not passport:
        raise HTTPException(status_code=HTTP_404_NOT_FOUND, detail="A passport with this id does not exist")

    # Return the passport photo 
    return Response(content=passport.photo)

@router.get("/credentials")
async def get_passport_credentials(
    passport_id: str,
    apikey : APIKey = Depends(validate_api_key),
    db_client: AsyncIOMotorClient = Depends(get_database)
):
    # Find the passport on the db
    passport = await async_dal.get_passport(db_client = db_client, passport_id=passport_id)
    
    # Handle missing passport 
    if not passport:
        raise HTTPException(status_code=HTTP_404_NOT_FOUND, detail="A passport with this id does not exist")

    # Call the issuer API to generate the connection
    fullname = passport.firstname + " " + passport.lastname
    res = IssuerService.initiate_connection(passport_id=passport.passport_id, fullname=fullname)
    
    # Retry until the qr code is available 
    remaining_retries = 25
    sleep_duration = 1
    qr_code_content = None
    while (qr_code_content is None and remaining_retries > 0):
        print("retrying")
        remaining_retries = remaining_retries -1
        sleep(sleep_duration)
        res = IssuerService.get_qr_code(passport_id=passport.passport_id, fullname=fullname)
        if res.status_code == 200:
            qr_code_content = res.content
    
    # Return the qr code if available
    if qr_code_content:
        return Response(content=qr_code_content)
    else:
        raise HTTPException(status_code=HTTP_404_NOT_FOUND, detail="Cannot retrieve the QR code, please try again later")

@router.delete("/passport", status_code=200)
async def delete_passport(
    passport_id: str,
    apikey : APIKey = Depends(validate_api_key),
    db_client: AsyncIOMotorClient = Depends(get_database)
):
    deleted = await async_dal.delete_passport(db_client = db_client, passport_id=passport_id)
    if (not deleted):
        raise HTTPException(status_code=HTTP_404_NOT_FOUND, detail="A passport with this id does not exist")

    return {"Response" : "Passport deleted"}