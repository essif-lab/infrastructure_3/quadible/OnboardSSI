from fastapi import Security,HTTPException
from fastapi.security import APIKeyHeader
from starlette.status import HTTP_401_UNAUTHORIZED
from common.config import get_settings

api_key_header = APIKeyHeader(name="x-api-key", auto_error=False)

async def validate_api_key(
    api_key_header: str = Security(api_key_header),
):

    settings = get_settings()
    correct_api_key = settings.MASTER_API_KEY
    
    if api_key_header == correct_api_key:
        return api_key_header
    else:
        raise HTTPException(
            status_code=HTTP_401_UNAUTHORIZED, detail="Could not validate x-api-key header"
        )
