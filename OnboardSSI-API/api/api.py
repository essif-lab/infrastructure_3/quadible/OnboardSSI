from fastapi import APIRouter
from api.endpoints import passport_collector

api_router = APIRouter()
api_router.include_router(passport_collector.router, prefix="/passport_collector", tags=["Passport Collector Endpoints"])
