# OnboardSSI-API
[![N|Solid](https://www.quadible.co.uk/wp-content/uploads/2019/11/Quadible-logo-128x128-300dpi.png)](http://www.quadible.co.uk)

Onboard SSI API enables easy passport verification and retrieval. It depends on well established open source projects:

* [FastAPI] - a microframework for Python APIs
* [Uvicorn] - a lightning-fast ASGI server, built on uvloop and httptools.
* [Motor] - an asynchronous Python driver for MongoDB
* [MongoDB] - a non relational database platform
* [Docker] - a set of platform as a service (PaaS) products that use OS-level virtualization to deliver software in packages called containers.

## API Documentation
- You can view the API Documentation [here](https://ssi.quadible.io/docs)
- You can also use this sample [Postman collection](https://www.postman.com/collections/df18364d0cac560c714d)

## First time setup for local development
The following have been tested with Python 3.9 and VSCode IDE.

### Setup the environment
Open a terminal window and execute the following commands:
```sh
python -m venv ssienv
source ssienv/bin/activate
pip install -r requirements.txt
```

### Setup local MongoDB using Docker
Before running the API locally, setup the local mongo db using docker:

```sh
docker compose up mongo 
```

Afterwards perform the mongo-db, first time configuration:
1. Connect to the MongoDB container via the Docker GUI (easiest) or cli
2. Enter the mongo shell by running `mongo`
3. Run the following commands in the mongo shell:

```js
rs.initiate();
const config = rs.config();
config.members[0].host = 'localhost:27017';
rs.reconfig(config);
```

### Debug the API
Finally use the 'Run API' option on VSCode to debug the API.

   [FastAPI]: <https://fastapi.tiangolo.com/>
   [uvicorn]: <https://www.uvicorn.org/>
   [Motor]: <https://motor.readthedocs.io/>
   [MongoDB]: <https://www.mongodb.com/>
   [Docker]: <https://www.docker.com/>

## Getting Verifiable Credentials
- Use the /api/v1/passport_collector/credentials endpoint to get a QR code for a specific passport id.

- Scan the QR code using connect.me app

- Before each time you want to get verifiable credentials you should go to connect.me app and delete any existing connections.

### Troubleshooting verifiable credentials

If the system seems to get stuck and not working properly perform the following steps on your browser:

1. Open the [restart url](https://issuer.quadible.io/restart)
It will show 502 message

2. Open this [the initiate endpoint](https://issuer.quadible.io/initiate?name=name1&passport=pass12)


3. Open [the get qr endpoint](https://issuer.quadible.io/getqr?name=name1&passport=pass12) multiple times until you see a QR Code

4. You are now ready to retry using the API