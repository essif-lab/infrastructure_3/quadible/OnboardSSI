import uvicorn
from api import initialize_app

if __name__ == "__main__":
    # This is only used to run locally
    # Uvicorn is used to run the application on Google App Engine. See entrypoint in app.yaml
    app = initialize_app()
    uvicorn.run(app, host="0.0.0.0", port=8088)
