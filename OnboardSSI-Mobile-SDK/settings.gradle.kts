pluginManagement {
    repositories {
        google()
        gradlePluginPortal()
        mavenCentral()
    }
}

rootProject.name = "OnboardSSI"
include(":androidApp")
include(":shared")
enableFeaturePreview("VERSION_CATALOGS")
