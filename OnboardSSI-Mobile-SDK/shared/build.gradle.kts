plugins {
    kotlin("multiplatform")
    kotlin("native.cocoapods")
    id("kotlinx-serialization")
    id("com.android.library")
    kotlin("kapt")
}

version = "1.0"

kotlin {
    android()
    iosX64()
    iosArm64()
    iosSimulatorArm64()

    cocoapods {
        summary = "Some description for the Shared Module"
        homepage = "Link to the Shared Module homepage"
        ios.deploymentTarget = "14.1"
        podfile = project.file("../iosApp/Podfile")
        framework {
            baseName = "shared"
        }
    }

    sourceSets {
        val commonMain by getting {
            dependencies {
                implementation(libs.koin.core)
                implementation(libs.coroutines.core)
                implementation(libs.bundles.ktor.common)
                implementation(libs.touchlab.stately)
                implementation(libs.kotlinx.dateTime)
                api(libs.touchlab.kermit)
            }
        }
        val commonTest by getting {
            dependencies {
                implementation(kotlin("test"))
            }
        }
        val androidMain by getting {
            dependencies {
                implementation(libs.bundles.camera)
                implementation(libs.bundles.mlkit)
                implementation(libs.koin.android)
                implementation("org.tensorflow:tensorflow-lite:2.4.0")
                implementation("org.tensorflow:tensorflow-lite-gpu:2.4.0")
                implementation("org.tensorflow:tensorflow-lite-support:0.1.0")
                implementation(libs.androidx.lifecycle.viewmodel)
                implementation(libs.ktor.client.okHttp)
                // VCX library
                implementation ("com.evernym:vcx:0.13.1-d69198c@aar")
                implementation ("net.java.dev.jna:jna:4.5.0@aar")

// optional, required to libVcx logger configuration
                implementation ("org.slf4j:slf4j-api:1.7.30")
                implementation("com.github.bright:slf4android:0.1.6"){
                    this.isTransitive = true
                }

// optional, required to work with CompletableFuture implementation used by libVcx
                implementation ("net.sourceforge.streamsupport:android-retrofuture:1.7.1")

                implementation ("androidx.core:core-ktx:1.3.1")
                implementation ("androidx.fragment:fragment-ktx:1.2.5")
                implementation ("androidx.appcompat:appcompat:1.1.0")
                implementation ("androidx.constraintlayout:constraintlayout:1.1.3")
                implementation ("androidx.lifecycle:lifecycle-extensions:2.2.0")
                implementation ("androidx.lifecycle:lifecycle-viewmodel-ktx:2.2.0")
                implementation ("com.google.android.material:material:1.1.0")
                implementation ("androidx.room:room-runtime:2.4.3")
                implementation ("androidx.room:room-ktx:2.4.3")
                implementation ("com.journeyapps:zxing-android-embedded:4.1.0")
                implementation ("com.squareup.okhttp3:okhttp:4.8.1")
                implementation ("com.squareup.okhttp3:logging-interceptor:4.8.1")
                implementation ("com.github.bumptech.glide:glide:4.11.0")
                // could probably provide own log level implementation
                api("com.github.bright:slf4android:0.1.6"){
                    this.isTransitive = true
                }
                implementation ("org.slf4j:slf4j-api:1.7.30")
                implementation ("net.java.dev.jna:jna:4.5.0@aar")

                implementation ("com.evernym:vcx:0.13.1-d69198c@aar")
                api ("net.sourceforge.streamsupport:android-retrofuture:1.7.1")
                api ("net.sourceforge.streamsupport:android-retrostreams:1.7.1")
                implementation ("de.adorsys.android:securestoragelibrary:1.1.0")
                implementation ("androidx.annotation:annotation:1.1.0")
                implementation ("androidx.annotation:annotation-experimental:1.0.0")
                implementation ("androidx.annotation:annotation-experimental-lint:1.0.0")
                implementation ("net.lingala.zip4j:zip4j:2.6.1")
                implementation ("com.squareup.okhttp3:okhttp:4.8.1")
                implementation ("com.squareup.okhttp3:logging-interceptor:4.8.1")
            }
        }
        val androidTest by getting
        val iosX64Main by getting
        val iosArm64Main by getting
        val iosSimulatorArm64Main by getting
        val iosMain by creating {
            dependsOn(commonMain)
            iosX64Main.dependsOn(this)
            iosArm64Main.dependsOn(this)
            iosSimulatorArm64Main.dependsOn(this)

            dependencies {
                implementation(libs.ktor.client.ios)
            }

        }
        val iosX64Test by getting
        val iosArm64Test by getting
        val iosSimulatorArm64Test by getting
        val iosTest by creating {
            dependsOn(commonTest)
            iosX64Test.dependsOn(this)
            iosArm64Test.dependsOn(this)
            iosSimulatorArm64Test.dependsOn(this)
        }
    }
}

dependencies {
    "kapt" ("androidx.room:room-compiler:2.4.3")
    "kapt" ("com.github.bumptech.glide:compiler:4.11.0")
    "kapt" ("com.android.databinding:compiler:3.1.4")
//

}

android {
    compileSdk = 31
    sourceSets["main"].manifest.srcFile("src/androidMain/AndroidManifest.xml")
    defaultConfig {
        minSdk = 24
        targetSdk = 31
    }
    buildFeatures {
        dataBinding = true
        viewBinding = true
    }
}