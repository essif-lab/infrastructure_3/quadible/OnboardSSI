package com.quadible.onboardssi.face

actual class Face {

    actual val trackingId: Int?
        get() = 0

    actual val leftEyeOpenProbability: Float?
        get() = 0f

    actual val rightEyeOpenProbability: Float?
        get() = 0f

    actual val width: Int
        get() = 0
}