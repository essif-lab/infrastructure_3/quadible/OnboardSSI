package com.quadible.onboardssi.face

expect class Face {

    val trackingId: Int?

    val leftEyeOpenProbability: Float?

    val rightEyeOpenProbability: Float?

    val width: Int

}