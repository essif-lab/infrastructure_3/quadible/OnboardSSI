package com.quadible.onboardssi.selectcountry

sealed class SelectCountryIntent {

    object LoadData : SelectCountryIntent()
}