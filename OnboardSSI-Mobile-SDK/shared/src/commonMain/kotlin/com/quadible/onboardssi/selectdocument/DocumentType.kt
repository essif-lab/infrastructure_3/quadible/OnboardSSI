package com.quadible.onboardssi.selectdocument

enum class DocumentType(val title: String) {

    PASSPORT("Passport"),

    NATIONAL_IDENTITY("National Identity"),

    DRIVER_LICENSE("Driver's License");
}