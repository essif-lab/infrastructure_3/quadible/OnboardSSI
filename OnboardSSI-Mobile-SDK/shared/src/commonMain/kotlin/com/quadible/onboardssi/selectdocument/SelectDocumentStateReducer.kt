package com.quadible.onboardssi.selectdocument

import kotlinx.coroutines.flow.*

class SelectDocumentStateReducer {

    private val documentTypesFlow: MutableStateFlow<List<DocumentType>> =
        MutableStateFlow(emptyList())

    val state: Flow<SelectDocumentState> =
        documentTypesFlow.mapLatest { SelectDocumentState(types = it) }

    fun reduce(documentTypes: List<DocumentType>) {
        documentTypesFlow.value = documentTypes
    }
}
