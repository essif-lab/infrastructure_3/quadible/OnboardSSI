package com.quadible.onboardssi.issue

sealed class IssueIntent {

    data class LoadData(val documentDetails: DocumentDetails) : IssueIntent()

    object IssueCredentials : IssueIntent()

    object ResetSubmission : IssueIntent()
}