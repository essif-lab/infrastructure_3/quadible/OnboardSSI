package com.quadible.onboardssi.issue

import kotlinx.serialization.Serializable

@Serializable
data class DocumentDetails(
    val documentNumber: String = "",
    val surname: String = "",
    val firstname: String = "",
    val birthday: String = "",
    val issueDay: String = "",
    val expiryDay: String = "",
    val birthPlace: String = "",
    val authority: String = "",
)