package com.quadible.onboardssi.selectcountry

import co.touchlab.kermit.Logger
import com.quadible.onboardssi.arch.BaseViewModel
import kotlinx.coroutines.flow.StateFlow

class SelectCountryViewModel(
    log: Logger
) : BaseViewModel<SelectCountryState, SelectCountryIntent>() {

    override val log = log.withTag(this::class.simpleName!!)

    private val reducer = SelectCountryStateReducer()

    override val state: StateFlow<SelectCountryState> = asStateFlow(
        flow = reducer.state,
        scope = viewModelScope
    )

    override suspend fun handleIntent(intent: SelectCountryIntent): Unit = when (intent) {
        SelectCountryIntent.LoadData -> handleLoadData()
    }

    private fun handleLoadData() {
        reducer.reduce(countries = Country.values().toList())
    }
}
