package com.quadible.onboardssi.issue

import co.touchlab.kermit.Logger
import com.quadible.onboardssi.arch.BaseViewModel
import com.quadible.onboardssi.filesystem.File
import com.quadible.onboardssi.scandocument.MRZ
import com.quadible.onboardssi.uuid.UUIDProvider
import io.ktor.client.*
import io.ktor.client.request.*
import io.ktor.client.request.forms.*
import io.ktor.http.*
import io.ktor.http.HttpStatusCode.Companion.Created
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class IssueViewModel(
    log: Logger,
    private val httpClient: HttpClient,
    private val uuidProvider: UUIDProvider,
) : BaseViewModel<IssueState, IssueIntent>() {

    override val log = log.withTag(this::class.simpleName!!)

    private val reducer = IssueStateReducer()

    override val state: StateFlow<IssueState> = asStateFlow(
        flow = reducer.state,
        scope = viewModelScope
    )

    override suspend fun handleIntent(intent: IssueIntent): Unit = when (intent) {
        is IssueIntent.LoadData -> handleLoadData(documentDetails = intent.documentDetails)
        IssueIntent.IssueCredentials -> handleIssue()
        IssueIntent.ResetSubmission -> handleResetSubmission()
    }

    private fun handleLoadData(documentDetails: DocumentDetails) {
        reducer.reduce(documentDetails = documentDetails)
    }

    private fun handleResetSubmission() {
        reducer.reduce(submissionState = SubmissionState.PENDING)
    }

    private fun handleIssue() {
        val exceptionHandler = loggingExceptionHandler + CoroutineExceptionHandler { _, _ ->
            reducer.reduce(submissionState = SubmissionState.FAILED)
        }

        viewModelScope.launch(exceptionHandler) {
            val currentState = state.value
            val response = httpClient.submitFormWithBinaryData(
                url = "https://ssi.quadible.io/api/v1/passport_collector/upload",
                formData = formData {
                    append("user_id", uuidProvider.generate())
                    append("firstname", currentState.firstname)
                    append("lastname", currentState.surname)
                    append("passport_id", currentState.passportNumber)
                    append("birthday", currentState.birthday)
                    append("issue_day", currentState.issueDay)
                    append("expiry_day", currentState.expiryDay)
                    append("birth_place", currentState.birthPlace)
                    append("authority", currentState.authority)

                    append("image", File("passport_0.jpeg").asByteArray(), Headers.build {
                        append(HttpHeaders.ContentType, "image/png")
                        append(HttpHeaders.ContentDisposition, "filename=\"passport_0.jpeg\"")
                    })
                }
            ) {
                headers {
                    append("x-api-key", "062F7A41-50E7-48FE-8A56-E47A30A9F049")
                }
            }

            val submissionState = when (response.status) {
                Created -> SubmissionState.SUCCESSFUL
                else -> SubmissionState.FAILED
            }
            reducer.reduce(submissionState = submissionState)
        }
    }
}
