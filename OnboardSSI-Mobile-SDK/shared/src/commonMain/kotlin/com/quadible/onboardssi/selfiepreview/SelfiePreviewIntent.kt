package com.quadible.onboardssi.selfiepreview

import com.quadible.onboardssi.face.similarity.PlatformImage

sealed class SelfiePreviewIntent {

    data class LoadData(
        val selfieImage: PlatformImage,
        val documentImage: PlatformImage,
    ) : SelfiePreviewIntent()
}