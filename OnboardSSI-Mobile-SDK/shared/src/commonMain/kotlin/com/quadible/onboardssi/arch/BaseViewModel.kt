package com.quadible.onboardssi.arch

import co.touchlab.kermit.Logger
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

abstract class BaseViewModel<State, Intent> : ViewModel() {

    internal abstract val log: Logger

    protected val loggingExceptionHandler = CoroutineExceptionHandler { _, throwable ->
        val message = throwable.message ?: throwable.stackTraceToString()
        log.e(message)
    }

    abstract val state: StateFlow<State>

    internal abstract suspend fun handleIntent(intent: Intent)

    fun processIntent(intent: Intent) {
        viewModelScope.launch(loggingExceptionHandler) { handleIntent(intent = intent) }
    }

    fun <T> asStateFlow(flow: Flow<T>, scope: CoroutineScope): StateFlow<T> =
        runBlocking { flow.stateIn(scope) }
}