package com.quadible.onboardssi.face

class BlinkingTrackerImpl : BlinkingTracker {

    companion object {

        private const val OPEN_THRESHOLD = 0.30f

        private const val CLOSE_THRESHOLD = 0.30f
    }

    private var state: BlinkingState = BlinkingState.INITIAL

    override fun onUpdate(face: Face) {
        val left = face.leftEyeOpenProbability ?: return
        val right = face.rightEyeOpenProbability ?: return

        val eyesOpenedProbability = EyesOpenedProbability(
                leftEyeProbability = left,
                rightEyeProbability = right
        )

        when {
            state.shouldMoveToOpenEyesState(eyesOpenedProbability) -> {
                // Both eyes are initially open
                state = BlinkingState.EYES_OPENED
            }
            state.shouldMoveToClosedEyesState(eyesOpenedProbability) -> {
                // Both eyes become closed
                state = BlinkingState.EYES_CLOSED
            }
            state.shouldMoveToInitialState(eyesOpenedProbability) -> {
                // Both eyes are open again
                state = BlinkingState.INITIAL
            }
        }
    }

    private fun BlinkingState.shouldMoveToOpenEyesState(
            eyesOpenedProbability: EyesOpenedProbability
    ): Boolean = this == BlinkingState.INITIAL && eyesOpenedProbability.areEyesOpened()

    private fun BlinkingState.shouldMoveToClosedEyesState(
            eyesOpenedProbability: EyesOpenedProbability
    ): Boolean = this == BlinkingState.EYES_OPENED && eyesOpenedProbability.areEyesClosed()

    private fun BlinkingState.shouldMoveToInitialState(
            eyesOpenedProbability: EyesOpenedProbability
    ): Boolean = this == BlinkingState.EYES_CLOSED && eyesOpenedProbability.areEyesOpened()

    private fun EyesOpenedProbability.areEyesOpened(): Boolean =
            leftEyeProbability > OPEN_THRESHOLD && rightEyeProbability > OPEN_THRESHOLD


    private fun EyesOpenedProbability.areEyesClosed(): Boolean =
            leftEyeProbability < CLOSE_THRESHOLD && rightEyeProbability < CLOSE_THRESHOLD
}

data class EyesOpenedProbability(val leftEyeProbability: Float, val rightEyeProbability: Float)

enum class BlinkingState {
    INITIAL, EYES_OPENED, EYES_CLOSED
}
