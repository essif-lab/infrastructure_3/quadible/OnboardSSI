package com.quadible.onboardssi.selfiepreview

class SelfiePreviewState(
    val faceSimilarity: FaceSimilarity,
)

enum class FaceSimilarity {
    PENDING, SIMILAR, NOT_SIMILAR;
}