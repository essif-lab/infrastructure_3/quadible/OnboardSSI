package com.quadible.onboardssi.issue

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.combine

class IssueStateReducer {

    private val documentDetailsFlow: MutableStateFlow<DocumentDetails> = MutableStateFlow(
        DocumentDetails(
            documentNumber = "",
            surname = "",
            firstname = "",
            birthday = "",
            issueDay = "",
            expiryDay = "",
            birthPlace = "",
            authority = "",
        )
    )

    private val submissionStateFlow: MutableStateFlow<SubmissionState> =
        MutableStateFlow(SubmissionState.PENDING)

    val state: Flow<IssueState> =
        documentDetailsFlow.combine(submissionStateFlow) { personalDetails, submissionState ->
            IssueState(
                passportNumber = personalDetails.documentNumber,
                surname = personalDetails.surname,
                firstname = personalDetails.firstname,
                birthday = personalDetails.birthday,
                issueDay = personalDetails.issueDay,
                expiryDay = personalDetails.expiryDay,
                birthPlace = personalDetails.birthPlace,
                authority = personalDetails.authority,
                submissionState = submissionState
            )
        }

    fun reduce(documentDetails: DocumentDetails) {
        documentDetailsFlow.value = documentDetails
    }

    fun reduce(submissionState: SubmissionState) {
        submissionStateFlow.value = submissionState
    }
}
