package com.quadible.onboardssi.filesystem

expect class File(name: String) {

    fun asByteArray(): ByteArray
}