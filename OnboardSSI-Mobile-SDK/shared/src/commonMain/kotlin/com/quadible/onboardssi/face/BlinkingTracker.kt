package com.quadible.onboardssi.face

interface BlinkingTracker {

    fun onUpdate(face: Face)
}