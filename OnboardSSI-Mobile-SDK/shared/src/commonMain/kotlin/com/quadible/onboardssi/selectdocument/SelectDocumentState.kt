package com.quadible.onboardssi.selectdocument

data class SelectDocumentState(
    val types: List<DocumentType>
)