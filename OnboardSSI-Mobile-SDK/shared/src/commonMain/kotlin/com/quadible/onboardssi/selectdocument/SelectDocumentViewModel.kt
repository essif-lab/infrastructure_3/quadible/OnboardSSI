package com.quadible.onboardssi.selectdocument

import co.touchlab.kermit.Logger
import com.quadible.onboardssi.arch.BaseViewModel
import kotlinx.coroutines.flow.StateFlow

class SelectDocumentViewModel(
    log: Logger
) : BaseViewModel<SelectDocumentState, SelectDocumentIntent>() {

    override val log = log.withTag(this::class.simpleName!!)

    private val reducer = SelectDocumentStateReducer()

    override val state: StateFlow<SelectDocumentState> = asStateFlow(
        flow = reducer.state,
        scope = viewModelScope
    )

    override suspend fun handleIntent(intent: SelectDocumentIntent): Unit = when (intent) {
        SelectDocumentIntent.LoadData -> handleLoadData()
    }

    private fun handleLoadData() {
        reducer.reduce(documentTypes = DocumentType.values().toList())
    }
}
