package com.quadible.onboardssi.face

import com.quadible.onboardssi.face.similarity.PlatformImage

expect class FaceCropper() {

    suspend fun crop(image: PlatformImage): PlatformImage

}