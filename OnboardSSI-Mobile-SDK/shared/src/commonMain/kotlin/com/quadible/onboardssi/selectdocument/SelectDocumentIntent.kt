package com.quadible.onboardssi.selectdocument

sealed class SelectDocumentIntent {

    object LoadData : SelectDocumentIntent()
}