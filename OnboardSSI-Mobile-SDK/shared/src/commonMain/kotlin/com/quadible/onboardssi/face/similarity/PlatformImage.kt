package com.quadible.onboardssi.face.similarity

expect class PlatformImage {

    fun save()

    fun flip(): PlatformImage

    fun scale(): PlatformImage
}