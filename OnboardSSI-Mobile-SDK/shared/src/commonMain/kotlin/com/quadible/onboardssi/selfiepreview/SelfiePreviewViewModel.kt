package com.quadible.onboardssi.selfiepreview

import co.touchlab.kermit.Logger
import com.quadible.onboardssi.arch.BaseViewModel
import com.quadible.onboardssi.face.FaceCropper
import com.quadible.onboardssi.face.similarity.FaceNetModel
import com.quadible.onboardssi.face.similarity.PlatformImage
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import kotlin.math.sqrt

class SelfiePreviewViewModel(
    log: Logger,
    private val faceNetModel: FaceNetModel,
    private val faceCropper: FaceCropper,
) : BaseViewModel<SelfiePreviewState, SelfiePreviewIntent>() {

    override val log = log.withTag(this::class.simpleName!!)

    private val reducer = SelfiePreviewStateReducer()

    override val state: StateFlow<SelfiePreviewState> = asStateFlow(
        flow = reducer.state,
        scope = viewModelScope
    )

    override suspend fun handleIntent(intent: SelfiePreviewIntent): Unit = when (intent) {
        is SelfiePreviewIntent.LoadData -> handleLoadData(
            selfieImage = intent.selfieImage,
            documentImage = intent.documentImage
        )
    }

    private fun handleLoadData(selfieImage: PlatformImage, documentImage: PlatformImage) {
        val exceptionHandler = loggingExceptionHandler + CoroutineExceptionHandler { _, _ ->
            reducer.reduce(faceSimilarity = FaceSimilarity.NOT_SIMILAR)
        }

        viewModelScope.launch(exceptionHandler) {
            val croppedDocument = faceCropper.crop(documentImage.flip())
            val croppedSelfie = faceCropper.crop(selfieImage)

            val distance = faceNetModel.compare(
                selfieImage = croppedSelfie.scale(),
                documentImage = croppedDocument.scale()
            )

            val faceSimilarity = if (distance > 0.8f) {
                FaceSimilarity.SIMILAR
            } else {
                FaceSimilarity.NOT_SIMILAR
            }

            reducer.reduce(faceSimilarity = faceSimilarity)
        }
    }
}
