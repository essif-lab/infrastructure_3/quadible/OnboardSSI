package com.quadible.onboardssi.face

class LargestFaceExtractorImpl : LargestFaceExtractor {

    override fun computeLargestFace(faces: List<Face>): Face? {
        var largestFace: Face? = null

        faces.onEach {
            val largestFaceWidth = largestFace?.width ?: 0
            val currentFaceWidth = it.width

            if (largestFaceWidth < currentFaceWidth) {
                largestFace = it
            }
        }

        return largestFace
    }
}
