package com.quadible.onboardssi.uuid

expect class UUIDProvider() {

    fun generate(): String
}