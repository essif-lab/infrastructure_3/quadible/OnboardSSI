package com.quadible.onboardssi.face

interface LargestFaceExtractor {

    fun computeLargestFace(faces: List<Face>): Face?
}
