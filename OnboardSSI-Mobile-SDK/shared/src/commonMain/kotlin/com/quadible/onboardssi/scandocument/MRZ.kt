package com.quadible.onboardssi.scandocument

data class MRZ(
    val mrzLine1: String,
    val mrzLine2: String,
)