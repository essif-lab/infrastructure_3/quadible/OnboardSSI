package com.quadible.onboardssi.selfiepreview

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.mapLatest

class SelfiePreviewStateReducer {

    private val faceSimilarityFlow: MutableStateFlow<FaceSimilarity> =
        MutableStateFlow(FaceSimilarity.PENDING)

    val state: Flow<SelfiePreviewState> =
        faceSimilarityFlow.mapLatest { SelfiePreviewState(faceSimilarity = it) }

    fun reduce(faceSimilarity: FaceSimilarity) {
        faceSimilarityFlow.value = faceSimilarity
    }
}
