package com.quadible.onboardssi.selectcountry

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.mapLatest

class SelectCountryStateReducer {

    private val countriesFlow: MutableStateFlow<List<Country>> =
        MutableStateFlow(emptyList())

    val state: Flow<SelectCountryState> =
        countriesFlow.mapLatest { SelectCountryState(types = it) }

    fun reduce(countries: List<Country>) {
        countriesFlow.value = countries
    }
}
