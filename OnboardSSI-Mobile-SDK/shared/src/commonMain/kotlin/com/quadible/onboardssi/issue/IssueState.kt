package com.quadible.onboardssi.issue

class IssueState(
    val passportNumber: String,
    val surname: String,
    val firstname: String,
    val birthday: String,
    val issueDay: String,
    val expiryDay: String,
    val birthPlace: String,
    val authority: String,
    val submissionState: SubmissionState,
)

enum class SubmissionState {
    PENDING, FAILED, SUCCESSFUL
}
