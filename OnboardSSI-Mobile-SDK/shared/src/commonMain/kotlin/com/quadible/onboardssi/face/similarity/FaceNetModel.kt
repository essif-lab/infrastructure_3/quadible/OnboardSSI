package com.quadible.onboardssi.face.similarity

expect class FaceNetModel {

    fun compare(selfieImage: PlatformImage, documentImage: PlatformImage): Float
}
