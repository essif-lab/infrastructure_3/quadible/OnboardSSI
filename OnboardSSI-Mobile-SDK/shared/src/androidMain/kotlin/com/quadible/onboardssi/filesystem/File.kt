package com.quadible.onboardssi.filesystem

import java.io.File as AndroidFile

actual class File actual constructor(name: String) {

    private val androidFile = AndroidFile(FileSystem.outputDirectory, name)

    actual fun asByteArray(): ByteArray = androidFile.readBytes()
}