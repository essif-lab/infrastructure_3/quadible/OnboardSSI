package com.quadible.onboardssi.scandocument

import android.annotation.SuppressLint
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.ImageProxy
import com.google.mlkit.vision.common.InputImage
import com.google.mlkit.vision.text.TextRecognition
import com.google.mlkit.vision.text.latin.TextRecognizerOptions
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

class MrzAnalyzer : ImageAnalysis.Analyzer {

    companion object {

        val PASSPORT_MRZ_LINE_1 = "(P[A-Z0-9<]{1})([A-Z]{3})([A-Z0-9<]{39})".toRegex()

        val PASSPORT_MRZ_LINE_2 =
            "([A-Z0-9<]{9})([0-9]{1})([A-Z]{3})([0-9]{6})([0-9]{1})([M|F|X|<]{1})([0-9]{6})([0-9]{1})([A-Z0-9<]{14})([0-9<]{1})([0-9]{1})".toRegex()
    }

    private val _result: MutableStateFlow<MrzAnalysisResult> =
        MutableStateFlow(MrzAnalysisResult.NotAvailable)
    val result: StateFlow<MrzAnalysisResult> = _result

    private val recognizer = TextRecognition.getClient(TextRecognizerOptions.DEFAULT_OPTIONS)

    private var counter = 0

    @SuppressLint("UnsafeOptInUsageError")
    override fun analyze(imageProxy: ImageProxy) {
        val mediaImage = imageProxy.image
        val rotationDegrees = imageProxy.imageInfo.rotationDegrees

        if (counter < 4) {
            counter++
            // Close imageProxy in order to get the next image. In other case the
            // analyzer will be blocked forever (till we finally close the imageProxy)
            imageProxy.close()
            return
        } else {
            counter++
        }

        if (mediaImage == null) {
            _result.value = MrzAnalysisResult.NotAvailable
            // Close imageProxy in order to get the next image. In other case the
            // analyzer will be blocked forever (till we finally close the imageProxy)
            imageProxy.close()
            return
        }

        val inputImage = InputImage.fromMediaImage(mediaImage, rotationDegrees)

        recognizer.process(inputImage)
            .addOnSuccessListener { result ->
                var mrzLine1 = ""
                var mrzLine2 = ""

                for (block in result.textBlocks) {
                    for (line in block.lines) {
                        for (element in line.elements) {
                            if (element.text.length < 42) {
                                // The sample is not good. Many missing letters
                                continue
                            }

                            // Some chars may be recognized in lower case form
                            // so we need to use .capitalize() here
                            var textToCheck = element.text.capitalize()
                            while (textToCheck.length < 44) {
                                // If we have multiple occurrences of '<' may confuse
                                // `TextRecognition`. As a result it may return a
                                // sequence of '<' with less occurrences of it. We this
                                // loop we fill the latest sequence (which is the safer)
                                val index = textToCheck.lastIndexOf('<')
                                if (index == -1) {
                                    break
                                }
                                textToCheck = textToCheck.replaceRange(index, index + 1, "<<")
                            }

                            if (textToCheck.matches(PASSPORT_MRZ_LINE_1)) {
                                mrzLine1 = textToCheck
                            } else if (textToCheck.matches(PASSPORT_MRZ_LINE_2)) {
                                mrzLine2 = textToCheck
                            }
                        }
                    }
                }

                val analysisResult = if (mrzLine1.isNotBlank() && mrzLine2.isNotBlank()) {
                    MrzAnalysisResult.Available(mrz = MRZ(mrzLine1 = mrzLine1, mrzLine2 = mrzLine2))
                } else {
                    MrzAnalysisResult.NotAvailable
                }
                _result.value = analysisResult

                // Close imageProxy in order to get the next image. In other case the
                // analyzer will be blocked forever (till we finally close the imageProxy)
                imageProxy.close()
            }
            .addOnFailureListener {
                _result.value = MrzAnalysisResult.NotAvailable
                // Close imageProxy in order to get the next image. In other case the
                // analyzer will be blocked forever (till we finally close the imageProxy)
                imageProxy.close()
            }
    }
}
