package com.quadible.onboardssi.db.dao

import androidx.room.*
import com.quadible.onboardssi.db.entity.Backup


@Dao
interface BackupDao {
    @Query("SELECT * FROM backup WHERE id = :id")
    suspend fun getById(id: Int): Backup?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(vararg backups: Backup)

    @Update
    suspend fun update(connection: Backup)

    @Delete
    suspend fun delete(connection: Backup)
}