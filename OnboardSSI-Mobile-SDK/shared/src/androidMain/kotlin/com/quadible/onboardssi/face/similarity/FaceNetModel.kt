package com.quadible.onboardssi.face.similarity

import android.content.Context
import android.content.res.AssetManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.graphics.Rect
import android.graphics.Point;
import org.tensorflow.lite.Interpreter
import java.io.FileInputStream
import java.io.IOException
import java.io.InputStream
import java.lang.Math.max
import java.lang.Math.min
import java.nio.MappedByteBuffer
import java.nio.channels.FileChannel

actual class FaceNetModel(
    private val context: Context,
) {

    private val MODEL_FILE = "MobileFaceNet.tflite"

    val INPUT_IMAGE_SIZE = 112 // 需要feed数据的placeholder的图片宽高

    val THRESHOLD = 0.8f // 设置一个阙值，大于这个值认为是同一个人

    private val interpreter by lazy  {
        val options = Interpreter.Options()
        options.setNumThreads(4)
        Interpreter(MyUtil.loadModelFile(context.assets, MODEL_FILE), options)
    }

    actual fun compare(selfieImage: PlatformImage, documentImage: PlatformImage): Float {
        // 将人脸resize为112X112大小的，因为下面需要feed数据的placeholder的形状是(2, 112, 112, 3)
        val bitmapScale1 =
            Bitmap.createScaledBitmap(selfieImage.bitmap, INPUT_IMAGE_SIZE, INPUT_IMAGE_SIZE, true)
        val bitmapScale2 =
            Bitmap.createScaledBitmap(documentImage.bitmap, INPUT_IMAGE_SIZE, INPUT_IMAGE_SIZE, true)
        val datasets = getTwoImageDatasets(bitmapScale1, bitmapScale2)
        val embeddings = Array(2) {
            FloatArray(
                192
            )
        }
        interpreter.run(datasets, embeddings)
        MyUtil.l2Normalize(embeddings, 1e-10)
        return evaluate(embeddings)
    }

    /**
     * 计算两张图片的相似度，使用l2损失
     * @param embeddings
     * @return
     */
    private fun evaluate(embeddings: Array<FloatArray>): Float {
        val embeddings1 = embeddings[0]
        val embeddings2 = embeddings[1]
        var dist = 0f
        for (i in 0..191) {
            dist += Math.pow((embeddings1[i] - embeddings2[i]).toDouble(), 2.0).toFloat()
        }
        var same = 0f
        for (i in 0..399) {
            val threshold = 0.01f * (i + 1)
            if (dist < threshold) {
                same += (1.0 / 400).toFloat()
            }
        }
        return same
    }

    /**
     * 转换两张图片为归一化后的数据
     * @param bitmap1
     * @param bitmap2
     * @return
     */
    private fun getTwoImageDatasets(
        bitmap1: Bitmap,
        bitmap2: Bitmap
    ): Array<Array<Array<FloatArray>>> {
        val bitmaps = arrayOf(bitmap1, bitmap2)
        val ddims = intArrayOf(bitmaps.size, INPUT_IMAGE_SIZE, INPUT_IMAGE_SIZE, 3)
        val datasets = Array(ddims[0]) {
            Array(ddims[1]) {
                Array(ddims[2]) {
                    FloatArray(ddims[3])
                }
            }
        }
        for (i in 0 until ddims[0]) {
            val bitmap = bitmaps[i]
            datasets[i] = MyUtil.normalizeImage(bitmap)
        }
        return datasets
    }
}


object MyUtil {
    /**
     * 从assets中读取图片
     * @param context
     * @param filename
     * @return
     */
    fun readFromAssets(context: Context, filename: String?): Bitmap? {
        val bitmap: Bitmap
        val asm = context.assets
        try {
            val `is`: InputStream = asm.open(filename!!)
            bitmap = BitmapFactory.decodeStream(`is`)
            `is`.close()
        } catch (e: IOException) {
            e.printStackTrace()
            return null
        }
        return bitmap
    }

    /**
     * 给rect增加margin
     * @param bitmap
     * @param rect
     * @param marginX
     * @param marginY
     */
    fun rectExtend(bitmap: Bitmap, rect: Rect, marginX: Int, marginY: Int) {
        rect.left = max(0, rect.left - marginX / 2)
        rect.right = min(bitmap.width - 1, rect.right + marginX / 2)
        rect.top = max(0, rect.top - marginY / 2)
        rect.bottom = min(bitmap.height - 1, rect.bottom + marginY / 2)
    }

    /**
     * 给rect增加margin
     * 使用长度不变，宽度增加到和长度一样
     * @param bitmap
     * @param rect
     */
    fun rectExtend(bitmap: Bitmap, rect: Rect) {
        val width: Int = rect.right - rect.left
        val height: Int = rect.bottom - rect.top
        val margin = (height - width) / 2
        rect.left = max(0, rect.left - margin)
        rect.right = min(bitmap.width - 1, rect.right + margin)
    }

    /**
     * 加载模型文件
     * @param assetManager
     * @param modelPath
     * @return
     * @throws IOException
     */
    fun loadModelFile(assetManager: AssetManager, modelPath: String?): MappedByteBuffer {
        val fileDescriptor = assetManager.openFd(modelPath!!)
        val inputStream = FileInputStream(fileDescriptor.fileDescriptor)
        val fileChannel: FileChannel = inputStream.getChannel()
        val startOffset = fileDescriptor.startOffset
        val declaredLength = fileDescriptor.declaredLength
        return fileChannel.map(FileChannel.MapMode.READ_ONLY, startOffset, declaredLength)
    }

    /**
     * 归一化图片到[-1, 1]
     * @param bitmap
     * @return
     */
    fun normalizeImage(bitmap: Bitmap): Array<Array<FloatArray>> {
        val h = bitmap.height
        val w = bitmap.width
        val floatValues = Array(h) {
            Array(w) {
                FloatArray(3)
            }
        }
        val imageMean = 127.5f
        val imageStd = 128f
        val pixels = IntArray(h * w)
        bitmap.getPixels(pixels, 0, bitmap.width, 0, 0, w, h)
        for (i in 0 until h) { // 注意是先高后宽
            for (j in 0 until w) {
                val `val` = pixels[i * w + j]
                val r = ((`val` shr 16 and 0xFF) - imageMean) / imageStd
                val g = ((`val` shr 8 and 0xFF) - imageMean) / imageStd
                val b = ((`val` and 0xFF) - imageMean) / imageStd
                val arr = floatArrayOf(r, g, b)
                floatValues[i][j] = arr
            }
        }
        return floatValues
    }

    /**
     * 缩放图片
     * @param bitmap
     * @param scale
     * @return
     */
    fun bitmapResize(bitmap: Bitmap, scale: Float): Bitmap {
        val width = bitmap.width
        val height = bitmap.height
        val matrix = Matrix()
        matrix.postScale(scale, scale)
        return Bitmap.createBitmap(
            bitmap, 0, 0, width, height, matrix, true
        )
    }

    /**
     * 图片矩阵宽高转置
     * @param in
     * @return
     */
    fun transposeImage(`in`: Array<Array<FloatArray>>): Array<Array<FloatArray>> {
        val h = `in`.size
        val w: Int = `in`[0].size
        val channel: Int = `in`[0][0].size
        val out = Array(w) {
            Array(h) {
                FloatArray(channel)
            }
        }
        for (i in 0 until h) {
            for (j in 0 until w) {
                out[j][i] = `in`[i][j]
            }
        }
        return out
    }

    /**
     * 4维图片batch矩阵宽高转置
     * @param in
     * @return
     */
    fun transposeBatch(`in`: Array<Array<Array<FloatArray>>>): Array<Array<Array<FloatArray>>> {
        val batch = `in`.size
        val h: Int = `in`[0].size
        val w: Int = `in`[0][0].size
        val channel: Int = `in`[0][0][0].size
        val out = Array(batch) {
            Array(w) {
                Array(h) {
                    FloatArray(channel)
                }
            }
        }
        for (i in 0 until batch) {
            for (j in 0 until h) {
                for (k in 0 until w) {
                    out[i][k][j] = `in`[i][j][k]
                }
            }
        }
        return out
    }

    /**
     * 截取box中指定的矩形框(越界要处理)，并resize到size*size大小，返回数据存放到data中。
     * @param bitmap
     * @param box
     * @param size
     * return
     */
    fun cropAndResize(bitmap: Bitmap?, box: Box, size: Int): Array<Array<FloatArray>> {
        // crop and resize
        val matrix = Matrix()
        val scaleW: Float = 1.0f * size / box.width()
        val scaleH: Float = 1.0f * size / box.height()
        matrix.postScale(scaleW, scaleH)
        val rect: Rect = box.transform2Rect()
        val croped = Bitmap.createBitmap(
            bitmap!!, rect.left, rect.top, box.width(), box.height(), matrix, true
        )
        return normalizeImage(croped)
    }

    /**
     * 按照rect的大小裁剪出人脸
     * @param bitmap
     * @param rect
     * @return
     */
    fun crop(bitmap: Bitmap?, rect: Rect): Bitmap {
        return Bitmap.createBitmap(
            bitmap!!,
            rect.left,
            rect.top,
            rect.right - rect.left,
            rect.bottom - rect.top
        )
    }

    /**
     * l2范数归一化
     * @param embeddings
     * @param epsilon 惩罚项
     * @return
     */
    fun l2Normalize(embeddings: Array<FloatArray>, epsilon: Double) {
        for (i in embeddings.indices) {
            var squareSum = 0f
            for (j in 0 until embeddings[i].size) {
                squareSum += Math.pow(embeddings[i][j].toDouble(), 2.0).toFloat()
            }
            val xInvNorm = Math.sqrt(Math.max(squareSum.toDouble(), epsilon)).toFloat()
            for (j in 0 until embeddings[i].size) {
                embeddings[i][j] = embeddings[i][j] / xInvNorm
            }
        }
    }

    /**
     * 图片转为灰度图
     * @param bitmap
     * @return 灰度图数据
     */
    fun convertGreyImg(bitmap: Bitmap): Array<IntArray> {
        val w = bitmap.width
        val h = bitmap.height
        val pixels = IntArray(h * w)
        bitmap.getPixels(pixels, 0, w, 0, 0, w, h)
        val result = Array(h) { IntArray(w) }
        val alpha = 0xFF shl 24
        for (i in 0 until h) {
            for (j in 0 until w) {
                val `val` = pixels[w * i + j]
                val red = `val` shr 16 and 0xFF
                val green = `val` shr 8 and 0xFF
                val blue = `val` and 0xFF
                var grey =
                    (red.toFloat() * 0.3 + green.toFloat() * 0.59 + blue.toFloat() * 0.11).toInt()
                grey = alpha or (grey shl 16) or (grey shl 8) or grey
                result[i][j] = grey
            }
        }
        return result
    }
}

class Box {
    var box // left:box[0],top:box[1],right:box[2],bottom:box[3]
            : IntArray
    var score // probability
            = 0f
    var bbr // bounding box regression
            : FloatArray
    var deleted: Boolean
    var landmark // facial landmark.只有ONet输出Landmark
            : Array<Point?>

    fun left(): Int {
        return box[0]
    }

    fun right(): Int {
        return box[2]
    }

    fun top(): Int {
        return box[1]
    }

    fun bottom(): Int {
        return box[2]
    }

    fun width(): Int {
        return box[2] - box[0] + 1
    }

    fun height(): Int {
        return box[3] - box[1] + 1
    }

    // 转为rect
    fun transform2Rect(): Rect {
        val rect = Rect()
        rect.left = box[0]
        rect.top = box[1]
        rect.right = box[2]
        rect.bottom = box[3]
        return rect
    }

    // 面积
    fun area(): Int {
        return width() * height()
    }

    // Bounding Box Regression
    fun calibrate() {
        val w = box[2] - box[0] + 1
        val h = box[3] - box[1] + 1
        box[0] = (box[0] + w * bbr[0]).toInt()
        box[1] = (box[1] + h * bbr[1]).toInt()
        box[2] = (box[2] + w * bbr[2]).toInt()
        box[3] = (box[3] + h * bbr[3]).toInt()
        for (i in 0..3) bbr[i] = 0.0f
    }

    // 当前box转为正方形
    fun toSquareShape() {
        val w = width()
        val h = height()
        if (w > h) {
            box[1] -= (w - h) / 2
            box[3] += (w - h + 1) / 2
        } else {
            box[0] -= (h - w) / 2
            box[2] += (h - w + 1) / 2
        }
    }

    // 防止边界溢出，并维持square大小
    fun limitSquare(w: Int, h: Int) {
        if (box[0] < 0 || box[1] < 0) {
            val len = max(-box[0], -box[1])
            box[0] += len
            box[1] += len
        }
        if (box[2] >= w || box[3] >= h) {
            val len = max(box[2] - w + 1, box[3] - h + 1)
            box[2] -= len
            box[3] -= len
        }
    }

    // 坐标是否越界
    fun transbound(w: Int, h: Int): Boolean {
        if (box[0] < 0 || box[1] < 0) {
            return true
        } else if (box[2] >= w || box[3] >= h) {
            return true
        }
        return false
    }

    init {
        box = IntArray(4)
        bbr = FloatArray(4)
        deleted = false
        landmark = arrayOfNulls<Point>(5)
    }
}