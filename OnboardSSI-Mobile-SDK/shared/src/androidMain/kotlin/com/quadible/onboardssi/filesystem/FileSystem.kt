package com.quadible.onboardssi.filesystem

import android.app.Application
import java.io.File

object FileSystem {

    private var appName: String = ""

    private var application: Application? = null

    fun init(application: Application, appName: String) {
        this.application = application
        this.appName = appName
    }

    val outputDirectory by lazy {
        File(application!!.filesDir, appName).apply { mkdirs() }.path
    }
}