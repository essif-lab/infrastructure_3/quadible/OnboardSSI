
package com.quadible.onboardssi.uuid

import java.util.*

actual class UUIDProvider actual constructor() {

    actual fun generate(): String = UUID.randomUUID().toString()
}