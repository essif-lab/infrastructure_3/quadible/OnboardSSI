package com.quadible.onboardssi.di

import com.quadible.onboardssi.face.similarity.FaceNetModel
import io.ktor.client.engine.okhttp.OkHttp
import org.koin.android.ext.koin.androidApplication
import org.koin.core.module.Module
import org.koin.dsl.module

actual val platformModule: Module = module {
    single {
        OkHttp.create()
    }

    single {
        FaceNetModel(get())
    }
}