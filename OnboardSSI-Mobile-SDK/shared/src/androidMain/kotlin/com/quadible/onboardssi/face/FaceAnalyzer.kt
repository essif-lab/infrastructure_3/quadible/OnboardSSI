package com.quadible.onboardssi.face

import android.annotation.SuppressLint
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.ImageProxy
import com.google.mlkit.vision.common.InputImage
import com.google.mlkit.vision.face.FaceDetection
import com.google.mlkit.vision.face.FaceDetectorOptions
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

class FaceAnalyzer : ImageAnalysis.Analyzer {

    companion object {
        private const val MAX_FRAME_ID = 4
    }

    private val highAccuracyOpts = FaceDetectorOptions.Builder()
        .setPerformanceMode(FaceDetectorOptions.PERFORMANCE_MODE_FAST)
        .setLandmarkMode(FaceDetectorOptions.LANDMARK_MODE_ALL)
        .setContourMode(FaceDetectorOptions.CONTOUR_MODE_NONE)
        .enableTracking()
        .setClassificationMode(FaceDetectorOptions.CLASSIFICATION_MODE_ALL)
        .setMinFaceSize(0.35f)
        .build()

    private val faceDetector = FaceDetection.getClient(highAccuracyOpts)

    private val _result: MutableStateFlow<FaceAnalysisResult> = MutableStateFlow(FaceAnalysisResult.NotAvailable)
    val result: StateFlow<FaceAnalysisResult> = _result

    private val largestFaceExtractor: LargestFaceExtractor = LargestFaceExtractorImpl()

    private val blinkingTracker: BlinkingTracker = BlinkingTrackerImpl()

    private var currentFrame = 0

    private var counter = 0

    private val executor: ExecutorService = Executors.newFixedThreadPool(4)

    @SuppressLint("UnsafeOptInUsageError")
    override fun analyze(imageProxy: ImageProxy) {
        val mediaImage = imageProxy.image

        if (counter < 4) {
            counter++
            // Close imageProxy in order to get the next image. In other case the
            // analyzer will be blocked forever (till we finally close the imageProxy)
            imageProxy.close()
            return
        } else {
            counter++
        }

        if (mediaImage == null) {
            _result.value = FaceAnalysisResult.NotAvailable
            // Close imageProxy in order to get the next image. In other case the
            // analyzer will be blocked forever (till we finally close the imageProxy)
            imageProxy.close()
            return
        }

        val rotationDegrees = imageProxy.imageInfo.rotationDegrees
        val inputImage = InputImage.fromMediaImage(mediaImage, rotationDegrees)

        faceDetector.process(inputImage).addOnSuccessListener { mlkitFaces ->
            executor.execute {
                val faces = mlkitFaces.map { Face(face = it) }
                val face = largestFaceExtractor.computeLargestFace(faces = faces)
                val faceId = face?.trackingId

                if (face == null || faceId == null) {
                    _result.value = FaceAnalysisResult.NotAvailable
                } else if (currentFrame >= MAX_FRAME_ID) {
                    blinkingTracker.onUpdate(face = face)

                    _result.value = FaceAnalysisResult.Available(face)
                } else {
                    currentFrame++
                }

                // Close imageProxy in order to get the next image. In other case the
                // analyzer will be blocked forever (till we finally close the imageProxy)
                imageProxy.close()
            }
        }.addOnFailureListener {
            _result.value = FaceAnalysisResult.NotAvailable
            // Close imageProxy in order to get the next image. In other case the
            // analyzer will be blocked forever (till we finally close the imageProxy)
            imageProxy.close()
        }
    }
}
