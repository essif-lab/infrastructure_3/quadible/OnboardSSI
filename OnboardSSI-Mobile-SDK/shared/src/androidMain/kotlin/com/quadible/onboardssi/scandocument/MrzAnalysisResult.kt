package com.quadible.onboardssi.scandocument

sealed class MrzAnalysisResult {

    object NotAvailable : MrzAnalysisResult()

    data class Available(val mrz: MRZ) : MrzAnalysisResult()
}