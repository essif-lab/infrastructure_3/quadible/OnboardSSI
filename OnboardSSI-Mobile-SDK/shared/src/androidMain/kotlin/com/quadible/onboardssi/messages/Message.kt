package com.quadible.onboardssi.messages

class Message(
    val pwDid: String,
    val uid: String,
    val payload: String,
    val type: String,
    val status: String
)