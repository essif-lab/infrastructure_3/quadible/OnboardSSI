package com.quadible.onboardssi.face.similarity

import android.graphics.Bitmap
import android.graphics.Matrix
import com.quadible.onboardssi.filesystem.FileSystem
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.*

actual class PlatformImage(val bitmap: Bitmap) {

    actual fun save() {
        try {
            val test = File(FileSystem.outputDirectory, "${UUID.randomUUID().toString()}.jpg")

            FileOutputStream(test).use { out ->
                bitmap.compress(
                    Bitmap.CompressFormat.PNG,
                    100,
                    out
                ) // bmp is your Bitmap instance
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    actual fun flip(): PlatformImage {
        // create new matrix for transformation
        val matrix = Matrix()
        matrix.preScale(-1.0f, 1.0f)

        val flippedBitmap =
            Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
        return PlatformImage(bitmap = flippedBitmap)
    }

    actual fun scale(): PlatformImage {
        val scaledBitmap = Bitmap.createScaledBitmap(bitmap, 160, 160, false)
        return PlatformImage(bitmap = scaledBitmap)
    }
}
