package com.quadible.onboardssi.face

import com.google.mlkit.vision.face.Face as MLKitFace

actual class Face(private val face: MLKitFace) {

    actual val trackingId: Int?
        get() = face.trackingId

    actual val leftEyeOpenProbability: Float?
        get() = face.leftEyeOpenProbability

    actual val rightEyeOpenProbability: Float?
        get() = face.rightEyeOpenProbability

    actual val width: Int
        get() = face.boundingBox.width()

}