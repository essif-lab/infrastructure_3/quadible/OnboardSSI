package com.quadible.onboardssi

object Constants {
    // SDK configuration settings
    const val AGENCY_ENDPOINT = "https://agency.pps.evernym.com"
    const val AGENCY_DID = "3mbwr7i85JNSL3LoNQecaW"
    const val AGENCY_VERKEY = "2WXxo6y1FJvXWgZnoYUP5BJej2mceFrqBDNPE3p6HDPf"
    const val WALLET_NAME = "wallet-name"
    const val LOGO = "https://robothash.com/logo.png"
    const val NAME = "MSDK Sample app"
    const val PROTOCOL_TYPE = "4.0"
    const val SERVER_URL: String = "http://192.168.1.4:4321" // put endpoint of your Sponsor server here

    // Application constants
    const val PREFS_NAME = "prefs"
    const val SPONSEE_ID = "sponsee_id"
}
