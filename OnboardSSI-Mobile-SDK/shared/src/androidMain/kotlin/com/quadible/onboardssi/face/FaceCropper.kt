package com.quadible.onboardssi.face

import android.graphics.Bitmap
import com.google.mlkit.vision.common.InputImage
import com.google.mlkit.vision.face.FaceDetection
import com.google.mlkit.vision.face.FaceDetectorOptions
import com.quadible.onboardssi.face.similarity.PlatformImage
import kotlinx.coroutines.suspendCancellableCoroutine

actual class FaceCropper actual constructor() {

    private val realTimeOpts = FaceDetectorOptions.Builder()
        .setPerformanceMode(FaceDetectorOptions.PERFORMANCE_MODE_FAST)
        .build()

    private val detector = FaceDetection.getClient(realTimeOpts)

    actual suspend fun crop(image: PlatformImage): PlatformImage =
        suspendCancellableCoroutine { continuation ->
            val bitmap = image.bitmap
            val inputImage = InputImage.fromByteArray(
                bitmapToNV21ByteArray(bitmap),
                bitmap.width,
                bitmap.height,
                0,
                InputImage.IMAGE_FORMAT_NV21
            )

            detector.process(inputImage)
                .addOnSuccessListener { faces ->
                    if (faces.size == 1) {
                        with(faces[0].boundingBox) {
                            try {
                                val croppedBitmap =
                                    Bitmap.createBitmap(
                                        bitmap,
                                        left,
                                        top,
                                        right - left,
                                        bottom - top
                                    )
                                continuation.resumeWith(Result.success(PlatformImage(bitmap = croppedBitmap)))
                            } catch (e: Exception) {
                                continuation.resumeWith(Result.failure(FailedToCropFaceException()))
                            }
                        }
                    } else {
                        continuation.resumeWith(Result.failure(FaceNotFoundException()))
                    }
                }.addOnFailureListener {
                    continuation.resumeWith(Result.failure(it))
                }

        }

    // Convert the given Bitmap to NV21 ByteArray
// See this comment -> https://github.com/firebase/quickstart-android/issues/932#issuecomment-531204396
    private fun bitmapToNV21ByteArray(bitmap: Bitmap): ByteArray {
        val argb = IntArray(bitmap.width * bitmap.height)
        bitmap.getPixels(argb, 0, bitmap.width, 0, 0, bitmap.width, bitmap.height)
        val yuv = ByteArray(
            bitmap.height * bitmap.width + 2 * Math.ceil(bitmap.height / 2.0).toInt()
                    * Math.ceil(bitmap.width / 2.0).toInt()
        )
        encodeYUV420SP(yuv, argb, bitmap.width, bitmap.height)
        return yuv
    }

    private fun encodeYUV420SP(yuv420sp: ByteArray, argb: IntArray, width: Int, height: Int) {
        val frameSize = width * height
        var yIndex = 0
        var uvIndex = frameSize
        var R: Int
        var G: Int
        var B: Int
        var Y: Int
        var U: Int
        var V: Int
        var index = 0
        for (j in 0 until height) {
            for (i in 0 until width) {
                R = argb[index] and 0xff0000 shr 16
                G = argb[index] and 0xff00 shr 8
                B = argb[index] and 0xff shr 0
                Y = (66 * R + 129 * G + 25 * B + 128 shr 8) + 16
                U = (-38 * R - 74 * G + 112 * B + 128 shr 8) + 128
                V = (112 * R - 94 * G - 18 * B + 128 shr 8) + 128
                yuv420sp[yIndex++] = (if (Y < 0) 0 else if (Y > 255) 255 else Y).toByte()
                if (j % 2 == 0 && index % 2 == 0) {
                    yuv420sp[uvIndex++] = (if (V < 0) 0 else if (V > 255) 255 else V).toByte()
                    yuv420sp[uvIndex++] = (if (U < 0) 0 else if (U > 255) 255 else U).toByte()
                }
                index++
            }
        }
    }

}

private class FailedToCropFaceException : Exception()

private class FaceNotFoundException : Exception()