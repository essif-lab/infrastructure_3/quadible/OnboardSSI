package com.quadible.onboardssi.db

enum class ActionStatus {
    PENDING,
    HISTORIZED
}