package com.quadible.onboardssi.face

sealed class FaceAnalysisResult {

    object NotAvailable : FaceAnalysisResult()

    data class Available(val face: Face) : FaceAnalysisResult()
}