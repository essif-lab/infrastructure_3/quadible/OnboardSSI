# OnboardSSI Mobile SDK
[![N|Solid](https://www.quadible.co.uk/wp-content/uploads/2019/11/Quadible-logo-128x128-300dpi.png)](http://www.quadible.co.uk)

Overview
===

This project constitutes a mobile wallet that incorporates the OnboardSSI Mobile SDK to allow strong identity verification with High Level of Assurance, the Evernym Mobile SDK to be able to interact with the SSI and the BehavAuth in order to offer continuous behavioural authentication.


Requirements
===
The OnboardSSI require for the opening of the project the Android Studio BumbleBee and for the iOS version the Xcode version 14.

For the deployment of the solution, the requirements are:

- Minimum Android SDK is 24
- Minimum iOS SDK is 13


Technology
===

In order to support both Android and iOS platform, the solution has been developed through the Kotlin Multiplatform Mobile framework.

Deployment
===

For the deployment of the Android version to a smartphone, there is a need to connect the smartphone to the computer, build and deploy the solution through the Android Studio. For the deployment of the iOS version there is a need to connect an iPhone, build and deploy the solution through the Xcode.
