package com.quadible.onboardssi

import android.net.Uri
import androidx.camera.core.CameraSelector
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.navigation.NamedNavArgument
import androidx.navigation.NavType
import androidx.navigation.navArgument
import com.quadible.onboardssi.face.FaceAnalysisResult
import com.quadible.onboardssi.face.FaceAnalyzer
import com.quadible.onboardssi.issue.DocumentDetails
import com.quadible.onboardssi.selectcountry.Country
import com.quadible.onboardssi.selectdocument.DocumentType
import kotlinx.serialization.*
import kotlinx.serialization.json.*
import java.net.URLEncoder

@Composable
fun SelfieScreen(
    navigateToPhotoPreview: (Uri) -> Unit
) {
    val isFaceDetected = remember { mutableStateOf(false) }
    val faceAnalyzer = remember { FaceAnalyzer() }
    LaunchedEffect(key1 = faceAnalyzer.result) {
        faceAnalyzer.result.collect { isFaceDetected.value = it is FaceAnalysisResult.Available }
    }

    CameraScreen(
        onTakePicture = { navigateToPhotoPreview(it) },
        canTakePicture = isFaceDetected.value,
        imageAnalyzer = faceAnalyzer,
        lensFacing = CameraSelector.LENS_FACING_FRONT,
    )
}

object SelfieDestination : NavigationDestination {

    const val documentTypeArg: String = "documentType"

    const val countryArg: String = "country"

    const val documentDetailsArg: String = "documentDetails"

    private const val baseRoute: String = "selfie"

    override val route: String = "${baseRoute}/{${documentTypeArg}}/{${countryArg}}/{${documentDetailsArg}}"

    fun routeForArguments(
        documentType: DocumentType,
        country: Country,
        documentDetails: DocumentDetails,
    ): String = "${baseRoute}/${documentType}/${country}/${
        URLEncoder.encode(Json.encodeToString(documentDetails), "utf-8")
    }"

    override val arguments: List<NamedNavArgument> = listOf(
        navArgument(documentTypeArg) {
            type = NavType.EnumType(DocumentType::class.java)
        },
        navArgument(countryArg) {
            type = NavType.EnumType(Country::class.java)
        },
        navArgument(documentDetailsArg) {
            type = NavType.StringType
        },
    )
}