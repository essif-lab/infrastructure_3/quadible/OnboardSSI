package com.quadible.onboardssi

import androidx.compose.runtime.Composable
import androidx.navigation.NamedNavArgument
import com.google.zxing.integration.android.IntentIntegrator

@Composable
fun QRcodeScreen(
    navigateToQRcodeScreen: () -> Unit
) {

}

object QRcodeDestination : NavigationDestination {

    private const val baseRoute: String = "qr-code"

    override val route: String = "${baseRoute}"

    override val arguments: List<NamedNavArgument> = emptyList()
}