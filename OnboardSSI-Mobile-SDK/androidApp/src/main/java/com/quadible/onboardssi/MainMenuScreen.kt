package com.quadible.onboardssi

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.material.*
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.painterResource
import androidx.navigation.NamedNavArgument

@Composable
fun MainMenuScreen(
    navigateToIntroScreen: () -> Unit,
    navigateToWalletScreen: () -> Unit,
    navigateToQRcodeScreen: () -> Unit
) {
    Column() {

        Column(
            modifier = Modifier.fillMaxSize().weight(1f),
            verticalArrangement =Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally,
        ) {

            Card(
                modifier = Modifier
                    .size(150.dp)
                    .testTag("circle"),
                shape = RoundedCornerShape(2.dp)
            ) {
                Image(
                    painterResource(R.drawable.quadible_logo),
                    contentDescription = "",
                    contentScale = ContentScale.Crop,
                    modifier = Modifier.fillMaxSize()
                )
            }

            Text(
                modifier = Modifier.padding(vertical = 24.dp, horizontal = 16.dp),
                text = "OnboardSSI",
                style = MaterialTheme.typography.h4
            )

            Divider()
        }

        OutlinedButton(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 16.dp).padding(bottom = 16.dp),
            onClick = { navigateToIntroScreen() }
        ) {
            Text(
                modifier = Modifier.padding(vertical = 10.dp),
                text = "Onboard Identity Document"
            )
        }

        OutlinedButton(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 16.dp).padding(bottom = 16.dp),
            onClick = { navigateToWalletScreen() }
        ) {
            Text(
                modifier = Modifier.padding(vertical = 10.dp),
                text = "Wallet"
            )
        }

        OutlinedButton(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 16.dp).padding(bottom = 16.dp),
            onClick = { navigateToQRcodeScreen() }
        ) {
            Text(
                modifier = Modifier.padding(vertical = 10.dp),
                text = "Scan QR code"
            )
        }

        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 16.dp),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Card(
                modifier = Modifier
                    .size(50.dp)
                    .testTag("circle"),
                shape = RoundedCornerShape(2.dp)
            ) {
                Image(
                    painterResource(R.drawable.eu_flag),
                    contentDescription = "",
                    contentScale = ContentScale.Crop,
                    modifier = Modifier.fillMaxSize()
                )
            }

            Text(
                modifier = Modifier.padding(vertical = 24.dp, horizontal = 16.dp),
                text = "This work has been funded by the European Commission through the eSSIF-Lab project, as part of the Horizon 2020 Research and Innovation Programme, under Grant Agreement Nº 871932 and it’s framed under Next Generation Internet Initiative.",
                style = MaterialTheme.typography.caption
            )
        }
    }
}

object MainMenuDestination : NavigationDestination {

    private const val baseRoute: String = "main-menu"

    override val route: String = "${baseRoute}"

    override val arguments: List<NamedNavArgument> = emptyList()
}