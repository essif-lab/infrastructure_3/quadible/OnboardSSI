package com.quadible.onboardssi

import androidx.navigation.NamedNavArgument

interface NavigationDestination {

    val route: String

    val arguments: List<NamedNavArgument>
}