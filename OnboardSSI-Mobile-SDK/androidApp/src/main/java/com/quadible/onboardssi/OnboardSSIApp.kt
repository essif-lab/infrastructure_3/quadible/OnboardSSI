package com.quadible.onboardssi

import android.app.Application
import android.content.Context
import com.quadible.onboardssi.di.initKoin
import com.quadible.onboardssi.issue.IssueViewModel
import com.quadible.onboardssi.selectcountry.SelectCountryViewModel
import com.quadible.onboardssi.selectdocument.SelectDocumentViewModel
import com.quadible.onboardssi.selfiepreview.SelfiePreviewViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.parameter.parametersOf
import org.koin.dsl.module

class OnboardSSIApp : Application() {

    override fun onCreate() {
        super.onCreate()
        initKoin(
            module {
                single<Context> { this@OnboardSSIApp }
                viewModel {
                    val parameters = { parametersOf("SelectDocumentViewModel") }
                    SelectDocumentViewModel(log = get(parameters = parameters))
                }
                viewModel {
                    val parameters = { parametersOf("SelectCountryViewModel") }
                    SelectCountryViewModel(log = get(parameters = parameters))
                }
                viewModel {
                    val parameters = { parametersOf("IssueViewModel") }
                    IssueViewModel(
                        log = get(parameters = parameters),
                        httpClient = get(parameters = parameters),
                        uuidProvider = get(parameters = parameters)
                    )
                }
                viewModel {
                    val parameters = { parametersOf("SelfiePreviewViewModel") }
                    SelfiePreviewViewModel(
                        log = get(parameters = parameters),
                        faceNetModel = get(parameters = parameters),
                        faceCropper = get(parameters = parameters)
                    )
                }
            }
        )
    }
}
