package com.quadible.onboardssi.history

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.quadible.onboardssi.db.ActionStatus.HISTORIZED
import com.quadible.onboardssi.db.Database
import com.quadible.onboardssi.db.entity.Action

class HistoryViewModel(application: Application) : AndroidViewModel(application) {
    private var db = Database.getInstance(application)
    private val actionsLiveData: LiveData<List<Action>> by lazy {
        db.actionDao().getActionsByStatus(HISTORIZED.toString())
    }

    fun getHistory(): LiveData<List<Action>> = actionsLiveData
}