package com.quadible.onboardssi

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.net.Uri
import android.util.Log
import android.util.Size
import android.view.Surface.ROTATION_0
import androidx.camera.core.*
import androidx.camera.core.CameraSelector.LensFacing
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.camera.view.PreviewView
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.viewinterop.AndroidView
import androidx.core.content.ContextCompat
import com.google.mlkit.vision.common.InputImage
import com.quadible.onboardssi.filesystem.FileSystem
import java.io.File
import java.io.FileOutputStream
import java.text.SimpleDateFormat
import java.util.*
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine


const val IMAGE_WIDTH = 720
const val IMAGE_HEIGHT = 1280
private const val FILENAME = "yyyy-MM-dd-HH-mm-ss-SSS"
private const val PHOTO_EXTENSION = ".jpg"

@Composable
fun CameraScreen(
    onTakePicture: (Uri) -> Unit,
    canTakePicture: Boolean,
    imageAnalyzer: ImageAnalysis.Analyzer,
    lensFacing: Int = CameraSelector.LENS_FACING_FRONT,
) {
    val imageCapture = remember {
        ImageCapture.Builder()
            .setTargetRotation(ROTATION_0)
            .setTargetResolution(Size(IMAGE_WIDTH, IMAGE_HEIGHT))
            .build()
    }

    CameraPreview(
        imageCapture = imageCapture,
        imageAnalyzer = imageAnalyzer,
        lensFacing = lensFacing
    )

    if (canTakePicture) {
        imageCapture.takePicture(
            context = LocalContext.current,
            onImageCaptured = onTakePicture
        )
    }
}

@Composable
private fun CameraPreview(
    imageCapture: ImageCapture,
    imageAnalyzer: ImageAnalysis.Analyzer,
    @LensFacing lensFacing: Int,
) {
    val context = LocalContext.current
    val lifecycleOwner = LocalLifecycleOwner.current
    val cameraExecutor = ContextCompat.getMainExecutor(context)

    val preview = remember { Preview.Builder().build() }

    val imageAnalysis = ImageAnalysis.Builder()
        .setBackpressureStrategy(ImageAnalysis.STRATEGY_KEEP_ONLY_LATEST)
        .setTargetResolution(Size(IMAGE_WIDTH, IMAGE_HEIGHT))
        .build()
    imageAnalysis.setAnalyzer(cameraExecutor, imageAnalyzer)

    val previewView = remember { PreviewView(context) }
    LaunchedEffect(lensFacing) {
        val cameraProvider = context.getCameraProvider()
        cameraProvider.unbindAll()
        cameraProvider.bindToLifecycle(
            lifecycleOwner,
            getCameraSelector(lensFacing),
            preview,
            imageAnalysis,
            imageCapture
        )
        preview.setSurfaceProvider(previewView.surfaceProvider)
    }
    AndroidView({ previewView }, modifier = Modifier.fillMaxSize())
}

private suspend fun Context.getCameraProvider(): ProcessCameraProvider =
    suspendCoroutine { continuation ->
        ProcessCameraProvider.getInstance(this).also { cameraProvider ->
            cameraProvider.addListener({
                continuation.resume(cameraProvider.get())
            }, ContextCompat.getMainExecutor(this))
        }
    }

private fun getCameraSelector(@LensFacing lensFacing: Int): CameraSelector = when (lensFacing) {
    CameraSelector.LENS_FACING_FRONT -> CameraSelector.DEFAULT_FRONT_CAMERA
    CameraSelector.LENS_FACING_BACK -> CameraSelector.DEFAULT_BACK_CAMERA
    else -> CameraSelector.Builder().build()
}

fun ImageCapture.takePicture(
    context: Context,
    onImageCaptured: (Uri) -> Unit,
) {
    val cameraExecutor = ContextCompat.getMainExecutor(context)
    val outputDirectory = FileSystem.outputDirectory
    // Create output file to hold the image
    val photoFile = createFile(outputDirectory, FILENAME, PHOTO_EXTENSION)

    takePicture(
        cameraExecutor,
        object : ImageCapture.OnImageCapturedCallback() {

            @SuppressLint("UnsafeOptInUsageError")
            override fun onCaptureSuccess(image: ImageProxy) {
                super.onCaptureSuccess(image)
                image.imageInfo.rotationDegrees

                val inputImage = InputImage.fromMediaImage(
                    image.image!!,
                    image.imageInfo.rotationDegrees
                )


                val fOut = FileOutputStream(photoFile)
                inputImage.bitmapInternal!!.compress(Bitmap.CompressFormat.JPEG, 100, fOut)
                fOut.flush()
                fOut.close()
                onImageCaptured(Uri.fromFile(photoFile))
            }
        })
}

fun createFile(baseFolder: String, format: String, extension: String) =
    File(
        baseFolder, SimpleDateFormat(format, Locale.US)
            .format(System.currentTimeMillis()) + extension
    )