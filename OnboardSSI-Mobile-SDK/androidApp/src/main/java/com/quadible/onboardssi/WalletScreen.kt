package com.quadible.onboardssi

import androidx.compose.runtime.Composable
import androidx.navigation.NamedNavArgument

@Composable
fun WalletScreen(
    navigateToQRcodeScreen: () -> Unit
) {

}

object WalletDestination : NavigationDestination {

    private const val baseRoute: String = "wallet"

    override val route: String = "${baseRoute}"

    override val arguments: List<NamedNavArgument> = emptyList()
}