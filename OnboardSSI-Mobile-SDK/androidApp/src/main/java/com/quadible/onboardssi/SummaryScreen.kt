package com.quadible.onboardssi

import android.widget.Toast
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Button
import androidx.compose.material.Card
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.SideEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.core.net.toUri
import androidx.navigation.NamedNavArgument
import androidx.navigation.NavType
import androidx.navigation.navArgument
import coil.compose.AsyncImage
import com.quadible.onboardssi.filesystem.FileSystem
import com.quadible.onboardssi.issue.DocumentDetails
import com.quadible.onboardssi.issue.IssueIntent
import com.quadible.onboardssi.issue.IssueViewModel
import com.quadible.onboardssi.issue.SubmissionState
import com.quadible.onboardssi.selectcountry.Country
import com.quadible.onboardssi.selectdocument.DocumentType
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.io.File
import java.net.URLEncoder

@Composable
fun SummaryScreen(
    viewModel: IssueViewModel,
    documentDetails: DocumentDetails,
    onSuccessfulSubmission: () -> Unit
) {
    val state = viewModel.state.collectAsState()

    SideEffect {
        viewModel.processIntent(intent = IssueIntent.LoadData(documentDetails = documentDetails))
    }

    Column {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(start = 30.dp, end = 30.dp, top = 100.dp, bottom = 16.dp)
                .weight(1f),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {

            Card(
                modifier = Modifier
                    .size(150.dp)
                    .testTag("circle"),
                shape = CircleShape,
                elevation = 2.dp
            ) {
                AsyncImage(
                    model = File(
                        FileSystem.outputDirectory,
                        "passport_0.jpeg"
                    ).toUri(),
                    contentDescription = "",
                    contentScale = ContentScale.Crop,
                    modifier = Modifier.fillMaxSize()
                )
            }

            Column(
                modifier = Modifier
                    .verticalScroll(rememberScrollState())
                    .padding(top = 50.dp)
            ) {


                OutlinedTextField(value = state.value.passportNumber,
                    onValueChange = {},
                    enabled = false,
                    label = {
                        Text(
                            modifier = Modifier.weight(1f),
                            text = "Passport No",
                            textAlign = TextAlign.Start
                        )
                    }
                )

                OutlinedTextField(value = state.value.surname, onValueChange = {}, enabled = false,
                    label = {
                        Text(
                            modifier = Modifier.weight(1f),
                            text = "Surname",
                            textAlign = TextAlign.Start
                        )
                    }
                )

                OutlinedTextField(value = state.value.firstname,
                    onValueChange = {},
                    enabled = false,
                    label = {
                        Text(
                            modifier = Modifier.weight(1f),
                            text = "Given names",
                            textAlign = TextAlign.Start
                        )
                    }
                )

                OutlinedTextField(value = state.value.birthday, onValueChange = {}, enabled = false,
                    label = {
                        Text(
                            modifier = Modifier.weight(1f),
                            text = "Date of birth",
                            textAlign = TextAlign.Start
                        )
                    }
                )

                OutlinedTextField(value = state.value.issueDay, onValueChange = {}, enabled = false,
                    label = {
                        Text(
                            modifier = Modifier.weight(1f),
                            text = "Date of issue",
                            textAlign = TextAlign.Start
                        )
                    }
                )

                OutlinedTextField(value = state.value.expiryDay,
                    onValueChange = {},
                    enabled = false,
                    label = {
                        Text(
                            modifier = Modifier.weight(1f),
                            text = "Date of expiry",
                            textAlign = TextAlign.Start
                        )
                    }
                )

                OutlinedTextField(value = state.value.birthPlace,
                    onValueChange = {},
                    enabled = false,
                    label = {
                        Text(
                            modifier = Modifier.weight(1f),
                            text = "Place of birth",
                            textAlign = TextAlign.Start
                        )
                    }
                )

                OutlinedTextField(value = state.value.authority,
                    onValueChange = {},
                    enabled = false,
                    label = {
                        Text(
                            modifier = Modifier.weight(1f),
                            text = "Authority",
                            textAlign = TextAlign.Start
                        )
                    }
                )
            }

        }
        Button(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 16.dp)
                .padding(bottom = 16.dp),
            onClick = { viewModel.processIntent(intent = IssueIntent.IssueCredentials) }
        ) {
            Text(
                modifier = Modifier.padding(vertical = 10.dp),
                text = "Submit to Issuer"
            )
        }

        when (state.value.submissionState) {
            SubmissionState.PENDING -> Unit
            SubmissionState.FAILED -> {
                Toast.makeText(
                    LocalContext.current,
                    "Creation failed",
                    Toast.LENGTH_LONG
                ).show()
                viewModel.processIntent(intent = IssueIntent.ResetSubmission)
            }
            SubmissionState.SUCCESSFUL -> {
                Toast.makeText(
                    LocalContext.current,
                    "Credentials were created",
                    Toast.LENGTH_LONG
                ).show()
                viewModel.processIntent(intent = IssueIntent.ResetSubmission)
                onSuccessfulSubmission()
            }
        }
    }
}

object SummaryDestination : NavigationDestination {

    const val documentTypeArg: String = "documentType"

    const val countryArg: String = "country"

    const val documentDetails: String = "documentDetails"

    private const val baseRoute: String = "summary"

    override val route: String = "${baseRoute}/{${documentTypeArg}}/{${countryArg}}/{${documentDetails}}"

    fun routeForArguments(
        documentType: DocumentType,
        country: Country,
        documentDetails: DocumentDetails,
    ): String = "${baseRoute}/${documentType}/${country}/${
        URLEncoder.encode(Json.encodeToString(documentDetails), "utf-8")
    }"

    override val arguments: List<NamedNavArgument> = listOf(
        navArgument(documentTypeArg) {
            type = NavType.EnumType(DocumentType::class.java)
        },
        navArgument(countryArg) {
            type = NavType.EnumType(Country::class.java)
        },
        navArgument(documentDetails) {
            type = NavType.StringType
        },
    )
}