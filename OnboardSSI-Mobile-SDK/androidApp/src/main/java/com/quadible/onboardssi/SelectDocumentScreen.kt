package com.quadible.onboardssi

import androidx.compose.foundation.Image
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Divider
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.SideEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.navigation.NamedNavArgument
import com.quadible.onboardssi.selectdocument.DocumentType
import com.quadible.onboardssi.selectdocument.SelectDocumentIntent
import com.quadible.onboardssi.selectdocument.SelectDocumentViewModel

@Composable
fun SelectDocumentList(
    viewModel: SelectDocumentViewModel,
    navigateToCountrySelection: (DocumentType) -> Unit,
) {
    val state = viewModel.state.collectAsState()

    SideEffect {
        viewModel.processIntent(intent = SelectDocumentIntent.LoadData)
    }

    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement =Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        Card(
            modifier = Modifier
                .size(150.dp)
                .testTag("circle"),
            shape = CircleShape,
            elevation = 2.dp
        ) {
            Image(
                painterResource(R.drawable.record),
                contentDescription = "",
                contentScale = ContentScale.Crop,
                modifier = Modifier.fillMaxSize()
            )
        }

        Text(
            modifier = Modifier.padding(vertical = 24.dp, horizontal = 16.dp),
            textAlign = TextAlign.Center,
            text = "Select identity document",
            style = MaterialTheme.typography.h4
        )

        Divider()
        state.value.types.forEach {
            SelectDocumentItem(
                onSelect = { navigateToCountrySelection(it) },
                type = it
            )
        }
    }
}

@Composable
private fun SelectDocumentItem(
    onSelect: () -> Unit,
    type: DocumentType
) {
    Spacer(
        modifier = Modifier.height(16.dp)
    )
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 16.dp)
            .clickable { onSelect() }
            .border(1.dp, color = Color.Blue, shape = RoundedCornerShape(8.dp))
    ) {
        Text(
            modifier = Modifier.padding(all = 16.dp),
            text = type.title,
            style = MaterialTheme.typography.body1
        )
    }
}

object SelectDocumentDestination : NavigationDestination {

    override val route: String = "select_document"

    override val arguments: List<NamedNavArgument> = emptyList()
}

