package com.quadible.onboardssi

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.LocalContext
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.google.zxing.integration.android.IntentIntegrator
import com.quadible.onboardssi.issue.DocumentDetails
import com.quadible.onboardssi.issue.IssueViewModel
import com.quadible.onboardssi.selectcountry.Country
import com.quadible.onboardssi.selectcountry.SelectCountryViewModel
import com.quadible.onboardssi.selectdocument.DocumentType
import com.quadible.onboardssi.selectdocument.SelectDocumentViewModel
import com.quadible.onboardssi.selfiepreview.SelfiePreviewViewModel
import kotlinx.coroutines.flow.StateFlow
import kotlinx.serialization.*
import kotlinx.serialization.json.Json

@Composable
fun SsiNavHost(
    newIntents: StateFlow<Intent>,
    selectDocumentViewModel: SelectDocumentViewModel,
    selectCountryViewModel: SelectCountryViewModel,
    issueViewModel: IssueViewModel,
    selfiePreviewViewModel: SelfiePreviewViewModel,
    navController: NavHostController = rememberNavController()
) {
    val context = LocalContext.current
    NavHost(
        navController = navController,
        startDestination = MainMenuDestination.route
    ) {
        composable(route = MainMenuDestination.route) {
            MainMenuScreen(
                navigateToIntroScreen = {
                    navController.navigate(IntroDestination.route)
                },
                navigateToWalletScreen = {
                    context.startActivity(Intent(context, MainActivity2::class.java))
                },
                navigateToQRcodeScreen = {
                    val intent = Intent(context, MainActivity2::class.java)
                    intent.putExtra(MainActivity2.BUNDLE_KEY_SHOULD_SCAN, true)
                    context.startActivity(intent)
                }
            )
        }
        composable(route = IntroDestination.route) {
            IntroScreen(
                navigateToPermissionScreen = {
                    navController.navigate(PermissionDestination.route)
                }
            )
        }
        composable(route = PermissionDestination.route) {
            PermissionScreen(
                navigateToDocumentType = {
                    navController.navigate(SelectDocumentDestination.route)
                }
            )
        }
        composable(route = SelectDocumentDestination.route) {
            SelectDocumentList(
                viewModel = selectDocumentViewModel,
                navigateToCountrySelection = {
                    navController.navigate(SelectCountryDestination.routeForArguments(it))
                }
            )
        }
        composable(
            route = SelectCountryDestination.route,
            arguments = SelectCountryDestination.arguments
        ) {
            val documentType: DocumentType =
                it.arguments!!.getEnum(SelectCountryDestination.documentTypeArg)

            SelectCountryScreen(
                viewModel = selectCountryViewModel,
                navigateToDocumentScanner = { country ->
                    navController.navigate(
                        IntroDocumentScannerDestination.routeForArguments(
                            documentType = documentType,
                            country = country
                        )
                    )
                }
            )
        }
        composable(
            route = IntroDocumentScannerDestination.route,
            arguments = IntroDocumentScannerDestination.arguments
        ) {
            val documentType: DocumentType =
                it.arguments!!.getEnum(IntroDocumentScannerDestination.documentTypeArg)
            val country: Country =
                it.arguments!!.getEnum(IntroDocumentScannerDestination.countryArg)
            IntroDocumentScannerScreen(
                navigateToDocumentScanner = {
                    navController.navigate(
                        ScanDocumentDestination.routeForArguments(
                            documentType = documentType,
                            country = country
                        )
                    )
                }
            )
        }
        composable(
            route = ScanDocumentDestination.route,
            arguments = ScanDocumentDestination.arguments
        ) {
            val documentType: DocumentType =
                it.arguments!!.getEnum(ScanDocumentDestination.documentTypeArg)
            val country: Country = it.arguments!!.getEnum(ScanDocumentDestination.countryArg)
            ScanDocumentScreen(
                navigateToPhotoPreview = { uri, mrzLine2 ->
                    navController.navigate(
                        DocumentPreviewDestination.routeForArguments(
                            documentType = documentType,
                            country = country,
                            uri = uri,
                            mrzLine2 = mrzLine2,
                        )
                    )
                }
            )
        }
        composable(
            route = NfcReaderDestination.route,
            arguments = NfcReaderDestination.arguments

        ) {
            val documentType: DocumentType =
                it.arguments!!.getEnum(NfcReaderDestination.documentTypeArg)
            val country: Country = it.arguments!!.getEnum(NfcReaderDestination.countryArg)
            val mrzLine2 = it.arguments!!.getString(NfcReaderDestination.mrzLine2Arg)!!

            NfcReaderScreen(
                newIntents = newIntents,
                mrzLine2 = mrzLine2,
                navigateToSelfie = { documentDetails ->
                    navController.navigate(
                        SelfieDestination.routeForArguments(
                            documentType = documentType,
                            country = country,
                            documentDetails = documentDetails,
                        )
                    )
                }
            )
        }
        composable(
            route = SelfieDestination.route,
            arguments = SelfieDestination.arguments
        ) {
            val documentType: DocumentType =
                it.arguments!!.getEnum(SelfieDestination.documentTypeArg)
            val country: Country = it.arguments!!.getEnum(SelfieDestination.countryArg)
            val documentDetails = it.arguments!!.getString(SelfieDestination.documentDetailsArg)!!

            SelfieScreen(
                navigateToPhotoPreview = { uri ->
                    navController.navigate(
                        SelfiePreviewDestination.routeForArguments(
                            documentType = documentType,
                            country = country,
                            uri = uri,
                            documentDetails = Json.decodeFromString(documentDetails),
                        )
                    )
                }
            )
        }
        composable(
            route = DocumentPreviewDestination.route,
            arguments = DocumentPreviewDestination.arguments
        ) {
            val documentType: DocumentType =
                it.arguments!!.getEnum(DocumentPreviewDestination.documentTypeArg)
            val country: Country = it.arguments!!.getEnum(DocumentPreviewDestination.countryArg)
            val uri = Uri.decode(it.arguments!!.getString(DocumentPreviewDestination.uriArg)!!)
            val mrzLine2 = it.arguments!!.getString(DocumentPreviewDestination.mrzLine2Arg)!!

            DocumentPreviewScreen(
                uri = Uri.parse(uri),
                onNavigate = {
                    navController.navigate(
                        NfcReaderDestination.routeForArguments(
                            documentType = documentType,
                            country = country,
                            mrzLine2 = mrzLine2
                        )
                    )
                },
                onNavigateUp = { navController.navigateUp() }
            )
        }
        composable(
            route = SelfiePreviewDestination.route,
            arguments = SelfiePreviewDestination.arguments
        ) {
            val documentType: DocumentType =
                it.arguments!!.getEnum(SelfiePreviewDestination.documentTypeArg)
            val country: Country = it.arguments!!.getEnum(SelfiePreviewDestination.countryArg)
            val uri = Uri.decode(it.arguments!!.getString(SelfiePreviewDestination.uriArg)!!)
            val documentDetails =
                it.arguments!!.getString(SelfiePreviewDestination.documentDetailsArg)!!

            SelfiePreviewScreen(
                viewModel = selfiePreviewViewModel,
                uri = Uri.parse(uri),
                onNavigate = {
                    navController.navigate(
                        SummaryDestination.routeForArguments(
                            documentType = documentType,
                            country = country,
                            documentDetails = Json.decodeFromString(documentDetails)
                        )
                    )
                },
                onNavigateUp = { navController.navigateUp() }
            )
        }
        composable(
            route = SummaryDestination.route,
            arguments = SummaryDestination.arguments,
        ) {
            val documentType: DocumentType =
                it.arguments!!.getEnum(SummaryDestination.documentTypeArg)
            val country: Country = it.arguments!!.getEnum(SummaryDestination.countryArg)
            val documentDetails = it.arguments!!.getString(SummaryDestination.documentDetails)!!

            SummaryScreen(
                viewModel = issueViewModel,
                documentDetails = Json.decodeFromString(documentDetails),
                onSuccessfulSubmission = {
                    navController.popBackStack(MainMenuDestination.route, false)
                }
            )
        }
    }
}

private fun <T> Bundle.getEnum(key: String): T = this.getSerializable(key) as T