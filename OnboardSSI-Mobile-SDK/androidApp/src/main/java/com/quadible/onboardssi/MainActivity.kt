package com.quadible.onboardssi

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.material.MaterialTheme
import com.quadible.behavauth.behavauthlib.AuthenticationCallback
import com.quadible.behavauth.behavauthlib.AuthenticationMode
import com.quadible.behavauth.behavauthlib.BehavAuth
import com.quadible.onboardssi.filesystem.FileSystem
import com.quadible.onboardssi.issue.IssueViewModel
import com.quadible.onboardssi.selectcountry.SelectCountryViewModel
import com.quadible.onboardssi.selectdocument.SelectDocumentViewModel
import com.quadible.onboardssi.selfiepreview.SelfiePreviewViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.component.KoinComponent

class MainActivity : AppCompatActivity(), KoinComponent {

    // Fixme check how we can add this fields as default values in composable
    //  like it is implemented with hilt. Declaring them here seems no scalable
    private val selectDocumentViewModel: SelectDocumentViewModel by viewModel()
    private val selectCountryViewModel: SelectCountryViewModel by viewModel()
    private val issueViewModel: IssueViewModel by viewModel()
    private val selfiePreviewViewModel: SelfiePreviewViewModel by viewModel()
    private val API_SECRET = "<to-be-added>"
    private val API_KEY = "<to-be-added>"

    private val newIntents by lazy { MutableStateFlow<Intent>(intent) }

    private val callback = object : AuthenticationCallback() {

        override fun onAuthenticationSucceeded() {
            Toast.makeText(this@MainActivity, "Authentication succeeded", Toast.LENGTH_LONG).show()
        }

        override fun onAutheticationFailed() {
            Toast.makeText(this@MainActivity, "Authentication failed", Toast.LENGTH_LONG).show()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        BehavAuth.getInstance().install(application, API_KEY, API_SECRET)
        BehavAuth.getInstance().setCallback(callback)

        FileSystem.init(application = application, appName = getString(R.string.app_name))
        setContent {
            MaterialTheme {
                SsiNavHost(
                    newIntents = newIntents,
                    selectDocumentViewModel = selectDocumentViewModel,
                    selectCountryViewModel = selectCountryViewModel,
                    issueViewModel = issueViewModel,
                    selfiePreviewViewModel = selfiePreviewViewModel,
                )
            }
        }
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        newIntents.value = intent
    }
}