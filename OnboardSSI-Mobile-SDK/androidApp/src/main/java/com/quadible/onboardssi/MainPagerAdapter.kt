package com.quadible.onboardssi

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.quadible.onboardssi.history.HistoryFragment
import com.quadible.onboardssi.homepage.HomePageFragment


class MainPagerAdapter(
    fm: FragmentManager,
    val shouldScan: Boolean,
) : FragmentPagerAdapter(fm) {
    override fun getItem(position: Int): Fragment = when (position) {
        0 -> HomePageFragment.newInstance(shouldScan = shouldScan)
        1 -> HistoryFragment.newInstance()
        else -> HomePageFragment.newInstance(shouldScan = shouldScan)
    }


    override fun getPageTitle(position: Int): CharSequence? = when (position) {
        0 -> "Home"
        1 -> "History"
        else -> null
    }

    override fun getCount(): Int = 2
}
