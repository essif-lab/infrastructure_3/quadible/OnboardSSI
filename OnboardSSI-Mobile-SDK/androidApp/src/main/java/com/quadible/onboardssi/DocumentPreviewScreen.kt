package com.quadible.onboardssi

import android.net.Uri
import androidx.compose.runtime.Composable
import androidx.navigation.NamedNavArgument
import androidx.navigation.NavType
import androidx.navigation.navArgument
import com.quadible.onboardssi.issue.DocumentDetails
import com.quadible.onboardssi.selectcountry.Country
import com.quadible.onboardssi.selectdocument.DocumentType
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.net.URLEncoder

@Composable
fun DocumentPreviewScreen(
    uri: Uri,
    onNavigate: () -> Unit,
    onNavigateUp: () -> Unit,
) {
    PhotoPreviewScreen(
        uri = uri,
        onNavigate = onNavigate,
        onNavigateUp = onNavigateUp
    )
}

object DocumentPreviewDestination : NavigationDestination {

    const val documentTypeArg: String = "documentType"

    const val countryArg: String = "country"

    const val uriArg: String = "uri"

    const val mrzLine2Arg: String = "mrzLine2"

    private const val baseRoute: String = "document_preview"

    override val route: String =
        "${baseRoute}/{${documentTypeArg}}/{${countryArg}}/{${mrzLine2Arg}}/{${uriArg}}"

    fun routeForArguments(
        documentType: DocumentType,
        country: Country,
        mrzLine2: String,
        uri: Uri,
    ): String {
        val encodedUri = Uri.encode(uri.toString())
        return "${baseRoute}/${documentType}/${country}/${mrzLine2}/${encodedUri}"
    }

    override val arguments: List<NamedNavArgument> = listOf(
        navArgument(documentTypeArg) {
            type = NavType.EnumType(DocumentType::class.java)
        },
        navArgument(countryArg) {
            type = NavType.EnumType(Country::class.java)
        },
        navArgument(uriArg) {
            type = NavType.StringType
        },
    )
}