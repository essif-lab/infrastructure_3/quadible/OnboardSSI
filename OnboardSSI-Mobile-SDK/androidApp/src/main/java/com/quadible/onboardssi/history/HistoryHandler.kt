package com.quadible.onboardssi.history

import com.quadible.onboardssi.SingleLiveData
import com.quadible.onboardssi.db.ActionStatus
import com.quadible.onboardssi.db.Database
import com.quadible.onboardssi.db.entity.Action
import com.quadible.onboardssi.homepage.Results

object HistoryHandler {
    suspend fun addHistoryAction(
        db: Database,
        name: String,
        description: String,
        icon: String,
        liveData: SingleLiveData<Results>
    ) {
        try {
            val action = Action(
                invite = null,
                name = name,
                description = description,
                icon = icon,
                status = ActionStatus.HISTORIZED.toString()
            )

            db.actionDao().insertAll(action)
            liveData.postValue(Results.ACTION_SUCCESS)
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
            liveData.postValue(Results.ACTION_FAILURE)
        }
    }

    suspend fun addToHistory(
        actionId: Int,
        description: String,
        db: Database,
        liveData: SingleLiveData<Results>
    ) {
        try {
            val action = db.actionDao().getActionsById(actionId)
            action.status = ActionStatus.HISTORIZED.toString()
            action.description = description
            db.actionDao().update(action)
            liveData.postValue(Results.REJECT)
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
            liveData.postValue(Results.FAILURE)
        }
    }
}