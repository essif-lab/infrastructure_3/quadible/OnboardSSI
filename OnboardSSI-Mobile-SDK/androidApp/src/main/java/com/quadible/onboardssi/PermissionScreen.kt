package com.quadible.onboardssi

import android.Manifest
import android.content.pm.PackageManager
import android.util.Log
import android.widget.Button
import android.widget.Toast
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.*
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.text.style.TextAlign
import androidx.core.content.ContextCompat
import androidx.navigation.NamedNavArgument
import androidx.navigation.NavType
import androidx.navigation.navArgument
import com.quadible.onboardssi.selectdocument.DocumentType


@Composable
fun PermissionScreen(
    navigateToDocumentType: () -> Unit
) {
    val context = LocalContext.current
    val launcher = rememberLauncherForActivityResult(
        ActivityResultContracts.RequestPermission()
    ) { isGranted: Boolean ->
        if (isGranted) {
            // Permission Accepted: Do something
            Log.d("ExampleScreen","PERMISSION GRANTED")
            navigateToDocumentType()

        } else {
            // Permission Denied: Do something
            Log.d("ExampleScreen","PERMISSION DENIED")
            Toast.makeText(context, "The permissions are required to proceed with the onboarding process.", Toast.LENGTH_LONG).show()
        }
    }

    Column() {

        Column(
            modifier = Modifier.fillMaxSize().weight(1f),
            verticalArrangement =Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally,
        ) {

            Card(
                modifier = Modifier
                    .size(150.dp)
                    .testTag("circle"),
                shape = CircleShape,
                elevation = 2.dp
            ) {
                Image(
                    painterResource(R.drawable.permissions),
                    contentDescription = "",
                    contentScale = ContentScale.Crop,
                    modifier = Modifier.fillMaxSize()
                )
            }

            Text(
                modifier = Modifier.padding(vertical = 24.dp, horizontal = 16.dp),
                textAlign = TextAlign.Center,
                text = "Data collected and Permissions",
                style = MaterialTheme.typography.h4
            )

            Divider()

            Text(
                modifier = Modifier.padding(vertical = 24.dp, horizontal = 16.dp),
                text = "OnboardSSI collects and processes personal data such as face image and the identity information detailed on your passport, in order to create digital credentials on SSI from the particular identity document. By continuing you agree with the collection and processing of your personal data.",
                style = MaterialTheme.typography.body1
            )
        }

        Button(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 16.dp).padding(bottom = 16.dp),
            onClick = {
                // Check permission
                when (PackageManager.PERMISSION_GRANTED) {
                    ContextCompat.checkSelfPermission(
                        context,
                        Manifest.permission.CAMERA
                    ) -> {
                        // Some works that require permission
                        Log.d("ExampleScreen","Code requires permission")
                        navigateToDocumentType()
                    }
                    else -> {
                        // Asking for permission
                        launcher.launch(Manifest.permission.CAMERA)
                    }
                }
            }
        ) {
            Text(
                modifier = Modifier.padding(vertical = 10.dp),
                text = "Next"
            )
        }
    }
}


object PermissionDestination : NavigationDestination {

    private const val baseRoute: String = "permissions"

    override val route: String = "${baseRoute}"

    override val arguments: List<NamedNavArgument> = emptyList()
}