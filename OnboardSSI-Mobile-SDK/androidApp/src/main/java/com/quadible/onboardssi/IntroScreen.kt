package com.quadible.onboardssi

import android.widget.Button
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.*
import androidx.compose.ui.platform.testTag
import androidx.navigation.NamedNavArgument
import androidx.navigation.NavType
import androidx.navigation.navArgument
import com.quadible.onboardssi.selectdocument.DocumentType


@Composable
fun IntroScreen(
    navigateToPermissionScreen: () -> Unit
) {
    Column() {

    Column(
        modifier = Modifier.fillMaxSize().weight(1f),
        verticalArrangement =Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {

        Card(
            modifier = Modifier
                .size(150.dp)
                .testTag("circle"),
            shape = CircleShape,
            elevation = 2.dp
        ) {
            Image(
                painterResource(R.drawable.onboardssi_logo),
                contentDescription = "",
                contentScale = ContentScale.Crop,
                modifier = Modifier.fillMaxSize()
            )
        }

        Text(
            modifier = Modifier.padding(vertical = 24.dp, horizontal = 16.dp),
            text = "OnboardSSI",
            style = MaterialTheme.typography.h4
        )

        Divider()
        
        Text(
            modifier = Modifier.padding(vertical = 24.dp, horizontal = 16.dp),
            text = "OnboardSSI allows you easily and securely verify your identity with High Level of Assurance (eIDAS) through onboarding your existing identity document such as a passport, to the SSI by generating the corresponding unique Verifiable Credentials.",
            style = MaterialTheme.typography.body1
        )
    }

    Button(
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 16.dp).padding(bottom = 16.dp),
        onClick = { navigateToPermissionScreen() }
    ) {
        Text(
            modifier = Modifier.padding(vertical = 10.dp),
            text = "Next"
        )
    }
    }
}

object IntroDestination : NavigationDestination {

    private const val baseRoute: String = "intro"

    override val route: String = "${baseRoute}"

    override val arguments: List<NamedNavArgument> = emptyList()
}