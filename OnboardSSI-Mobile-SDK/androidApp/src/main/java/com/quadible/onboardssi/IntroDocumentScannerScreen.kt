package com.quadible.onboardssi

import android.widget.Button
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.*
import androidx.compose.ui.platform.testTag
import androidx.navigation.NamedNavArgument
import androidx.navigation.NavType
import androidx.navigation.navArgument
import com.quadible.onboardssi.selectcountry.Country
import com.quadible.onboardssi.selectdocument.DocumentType


@Composable
fun IntroDocumentScannerScreen(
    navigateToDocumentScanner: () -> Unit
) {
    Column() {

    Column(
        modifier = Modifier.fillMaxSize().weight(1f),
        verticalArrangement =Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {

        Card(
            modifier = Modifier
                .size(150.dp)
                .testTag("circle"),
            shape = CircleShape,
            elevation = 2.dp
        ) {
            Image(
                painterResource(R.drawable.profile),
                contentDescription = "",
                contentScale = ContentScale.Crop,
                modifier = Modifier.fillMaxSize()
            )
        }

        Text(
            modifier = Modifier.padding(vertical = 24.dp, horizontal = 16.dp),
            text = "Take document photo",
            style = MaterialTheme.typography.h4
        )

        Divider()
        
        Text(
            modifier = Modifier.padding(vertical = 24.dp, horizontal = 16.dp),
            text = "Open your identity document, with visible the identity information and OnboardSSI will automatically read the information of the document. Please ensure that there are good light conditions so that we can read your document information.",
            style = MaterialTheme.typography.body1
        )
    }

    Button(
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 16.dp).padding(bottom = 16.dp),
        onClick = { navigateToDocumentScanner() }
    ) {
        Text(
            modifier = Modifier.padding(vertical = 10.dp),
            text = "Next"
        )
    }
    }
}

object IntroDocumentScannerDestination : NavigationDestination {

    private const val baseRoute: String = "intro-document-scanner"

    const val documentTypeArg: String = "documentType"

    const val countryArg: String = "country"

    override val route: String = "${baseRoute}/{${documentTypeArg}}/{${countryArg}}"

    fun routeForArguments(
        documentType: DocumentType,
        country: Country
    ): String = "${baseRoute}/${documentType}/${country}"

    override val arguments: List<NamedNavArgument> = listOf(
        navArgument(documentTypeArg) {
            type = NavType.EnumType(DocumentType::class.java)
        },
        navArgument(countryArg) {
            type = NavType.EnumType(Country::class.java)
        }
    )
}