package com.quadible.onboardssi

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Card
import androidx.compose.material.Divider
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.SideEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.navigation.NamedNavArgument
import androidx.navigation.NavType
import androidx.navigation.navArgument
import com.quadible.onboardssi.selectcountry.Country
import com.quadible.onboardssi.selectcountry.SelectCountryIntent
import com.quadible.onboardssi.selectcountry.SelectCountryViewModel
import com.quadible.onboardssi.selectdocument.DocumentType

@Composable
fun SelectCountryScreen(
    viewModel: SelectCountryViewModel,
    navigateToDocumentScanner: (Country) -> Unit
) {
    val state = viewModel.state.collectAsState()

    SideEffect {
        viewModel.processIntent(intent = SelectCountryIntent.LoadData)
    }

    Column(
        modifier = Modifier
            .fillMaxSize().padding(top = 100.dp),
        verticalArrangement =Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Card(
            modifier = Modifier
                .size(150.dp)
                .testTag("circle"),
            shape = CircleShape,
            elevation = 2.dp
        ) {
            Image(
                painterResource(R.drawable.location),
                contentDescription = "",
                contentScale = ContentScale.Crop,
                modifier = Modifier.fillMaxSize()
            )
        }

        Text(
            modifier = Modifier.padding(vertical = 24.dp, horizontal = 16.dp),
            textAlign = TextAlign.Center,
            text = "Select document country",
            style = MaterialTheme.typography.h4
        )

        Divider()
    Column(
        modifier = Modifier
            .verticalScroll(rememberScrollState())
    ) {

        state.value.types.forEach {
            SelectCountryItem(
                onSelect = { navigateToDocumentScanner(it) },
                country = it
            )
        }
    }
    }
}

@Composable
fun SelectCountryItem(
    onSelect: () -> Unit,
    country: Country
) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .clickable { onSelect() }
    ) {
        Text(
            modifier = Modifier.padding(all = 16.dp),
            text = country.title,
            style = MaterialTheme.typography.body1
        )
    }
}

object SelectCountryDestination : NavigationDestination {

    const val documentTypeArg = "documentType"

    private const val baseRoute: String = "select_country"

    override val route: String = "${baseRoute}/{${documentTypeArg}}"

    fun routeForArguments(documentType: DocumentType): String = "${baseRoute}/${documentType}"

    override val arguments: List<NamedNavArgument> = listOf(
        navArgument(documentTypeArg) {
            type = NavType.EnumType(DocumentType::class.java)
        }
    )
}