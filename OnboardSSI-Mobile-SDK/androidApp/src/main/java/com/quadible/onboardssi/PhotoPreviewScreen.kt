package com.quadible.onboardssi

import android.net.Uri
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Button
import androidx.compose.material.OutlinedButton
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NamedNavArgument
import androidx.navigation.NavType
import androidx.navigation.navArgument
import coil.compose.AsyncImage
import com.quadible.onboardssi.issue.DocumentDetails
import com.quadible.onboardssi.selectcountry.Country
import com.quadible.onboardssi.selectdocument.DocumentType
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.net.URLEncoder

@Composable
fun PhotoPreviewScreen(
    uri: Uri,
    onNavigate: () -> Unit,
    onNavigateUp: () -> Unit,
) {
    Column() {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .weight(1f),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            AsyncImage(
                modifier = Modifier.padding(top = 24.dp),
                model = uri,
                contentDescription = null
            )

            Text(
                modifier = Modifier.padding(vertical = 24.dp, horizontal = 16.dp),
                text = "Make sure your photo is clear"
            )
        }

        Button(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 16.dp)
                .padding(bottom = 16.dp),
            onClick = onNavigate
        ) {
            Text(
                modifier = Modifier.padding(vertical = 10.dp),
                text = "Photo is clear"
            )
        }

        OutlinedButton(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 16.dp)
                .padding(bottom = 16.dp),
            onClick = onNavigateUp
        ) {
            Text(
                modifier = Modifier.padding(vertical = 10.dp),
                text = "Take new picture"
            )
        }
    }
}

enum class PhotoType {
    DOCUMENT, SELFIE
}
