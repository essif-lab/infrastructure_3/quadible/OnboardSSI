package com.quadible.onboardssi

import android.graphics.Bitmap
import android.net.Uri
import android.provider.MediaStore
import android.util.Log
import android.widget.Toast
import androidx.compose.runtime.Composable
import androidx.compose.runtime.SideEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.platform.LocalContext
import androidx.core.net.toUri
import androidx.navigation.NamedNavArgument
import androidx.navigation.NavType
import androidx.navigation.navArgument
import com.quadible.onboardssi.face.similarity.PlatformImage
import com.quadible.onboardssi.filesystem.FileSystem
import com.quadible.onboardssi.issue.DocumentDetails
import com.quadible.onboardssi.selectcountry.Country
import com.quadible.onboardssi.selectdocument.DocumentType
import com.quadible.onboardssi.selfiepreview.FaceSimilarity
import com.quadible.onboardssi.selfiepreview.SelfiePreviewIntent
import com.quadible.onboardssi.selfiepreview.SelfiePreviewViewModel
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.net.URLEncoder
import java.util.*


@Composable
fun SelfiePreviewScreen(
    viewModel: SelfiePreviewViewModel,
    uri: Uri,
    onNavigate: () -> Unit,
    onNavigateUp: () -> Unit,
) {

    val state = viewModel.state.collectAsState()
    val context = LocalContext.current
    val contentResolver = context.contentResolver
    SideEffect {
        val selfieBitmap = MediaStore.Images.Media.getBitmap(contentResolver, uri)
        val selfieImage = PlatformImage(bitmap = selfieBitmap)

        val documentBitmapUri = File(FileSystem.outputDirectory, "passport_0.jpeg").toUri()
        val documentBitmap = MediaStore.Images.Media.getBitmap(contentResolver, documentBitmapUri)
        val documentImage = PlatformImage(bitmap = documentBitmap)

        viewModel.processIntent(
            intent = SelfiePreviewIntent.LoadData(selfieImage, documentImage)
        )
    }

    PhotoPreviewScreen(
        uri = uri,
        onNavigate = {
            when (state.value.faceSimilarity) {
                FaceSimilarity.PENDING -> Toast.makeText(
                    context,
                    "Processing face. Please wait",
                    Toast.LENGTH_LONG
                ).show()
                FaceSimilarity.SIMILAR -> onNavigate()
                FaceSimilarity.NOT_SIMILAR -> Toast.makeText(
                    context,
                    "Faces do not match. Please take photo again",
                    Toast.LENGTH_LONG
                ).show()
            }

        },
        onNavigateUp = onNavigateUp,
    )
}

object SelfiePreviewDestination : NavigationDestination {

    const val documentTypeArg: String = "documentType"

    const val countryArg: String = "country"

    const val uriArg: String = "uri"

    const val documentDetailsArg: String = "documentDetails"

    private const val baseRoute: String = "photo_preview"

    override val route: String =
        "${baseRoute}/{${documentTypeArg}}/{${countryArg}}/{${uriArg}}/{${documentDetailsArg}}"

    fun routeForArguments(
        documentType: DocumentType,
        country: Country,
        uri: Uri,
        documentDetails: DocumentDetails,
    ): String {
        val encodedUri = Uri.encode(uri.toString())
        return "${baseRoute}/${documentType}/${country}/${encodedUri}/${
            URLEncoder.encode(Json.encodeToString(documentDetails), "utf-8")
        }"
    }

    override val arguments: List<NamedNavArgument> = listOf(
        navArgument(documentTypeArg) {
            type = NavType.EnumType(DocumentType::class.java)
        },
        navArgument(countryArg) {
            type = NavType.EnumType(Country::class.java)
        },
        navArgument(uriArg) {
            type = NavType.StringType
        },
        navArgument(documentDetailsArg) {
            type = NavType.StringType
        },
    )
}
