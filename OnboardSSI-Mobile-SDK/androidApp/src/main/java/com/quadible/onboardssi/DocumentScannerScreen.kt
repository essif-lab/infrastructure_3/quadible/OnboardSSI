package com.quadible.onboardssi

import android.net.Uri
import androidx.camera.core.CameraSelector
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.navigation.NamedNavArgument
import androidx.navigation.NavType
import androidx.navigation.navArgument
import com.quadible.onboardssi.scandocument.MrzAnalysisResult
import com.quadible.onboardssi.scandocument.MrzAnalyzer
import com.quadible.onboardssi.selectcountry.Country
import com.quadible.onboardssi.selectdocument.DocumentType
import kotlinx.coroutines.flow.collectLatest

@Composable
fun ScanDocumentScreen(
    navigateToPhotoPreview: (Uri, String) -> Unit
) {
    val mrzLine2 = remember { mutableStateOf("") }
    val isMrzDetected = remember { mutableStateOf(false) }
    val mrzAnalyzer = remember { MrzAnalyzer() }
    LaunchedEffect(key1 = mrzAnalyzer.result) {
        mrzAnalyzer.result.collectLatest {
            when (it) {
                is MrzAnalysisResult.Available -> {
                    isMrzDetected.value = true
                    mrzLine2.value = it.mrz.mrzLine2
                }
                MrzAnalysisResult.NotAvailable -> {
                    isMrzDetected.value = false
                    mrzLine2.value = ""
                }
            }
        }
    }

    CameraScreen(
        onTakePicture = {
            if (mrzLine2.value.isNotBlank()) {
                navigateToPhotoPreview(it, mrzLine2.value)
            }
        },
        canTakePicture = isMrzDetected.value,
        imageAnalyzer = mrzAnalyzer,
        lensFacing = CameraSelector.LENS_FACING_BACK,
    )
}

object ScanDocumentDestination : NavigationDestination {

    const val documentTypeArg: String = "documentType"

    const val countryArg: String = "country"

    private const val baseRoute: String = "scan_document"

    override val route: String = "${baseRoute}/{${documentTypeArg}}/{${countryArg}}"

    fun routeForArguments(
        documentType: DocumentType,
        country: Country
    ): String = "${baseRoute}/${documentType}/${country}"

    override val arguments: List<NamedNavArgument> = listOf(
        navArgument(documentTypeArg) {
            type = NavType.EnumType(DocumentType::class.java)
        },
        navArgument(countryArg) {
            type = NavType.EnumType(Country::class.java)
        }
    )
}