package com.quadible.onboardssi

import android.app.PendingIntent
import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.nfc.NfcAdapter
import android.nfc.Tag
import android.nfc.tech.IsoDep
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Card
import androidx.compose.material.Divider
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.navigation.NamedNavArgument
import androidx.navigation.NavType
import androidx.navigation.navArgument
import com.quadible.onboardssi.ImageUtil.saveImage
import com.quadible.onboardssi.filesystem.FileSystem
import com.quadible.onboardssi.issue.DocumentDetails
import com.quadible.onboardssi.selectcountry.Country
import com.quadible.onboardssi.selectdocument.DocumentType
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collectLatest
import net.sf.scuba.smartcards.CardService
import org.jmrtd.BACKey
import org.jmrtd.PassportService
import org.jmrtd.PassportService.DEFAULT_MAX_BLOCKSIZE
import org.jmrtd.PassportService.NORMAL_MAX_TRANCEIVE_LENGTH
import org.jmrtd.lds.CardSecurityFile
import org.jmrtd.lds.PACEInfo
import org.jmrtd.lds.icao.DG1File
import org.jmrtd.lds.icao.DG2File
import java.io.File
import java.text.ParseException
import java.text.SimpleDateFormat

@Composable
fun NfcReaderScreen(
    newIntents: StateFlow<Intent>,
    mrzLine2: String,
    navigateToSelfie: (DocumentDetails) -> Unit,
) {
    val context = LocalContext.current
    LaunchedEffect(true) {
        nfcRead(context = context)
        newIntents.collectLatest {
            onScan(
                context = context,
                intent = it,
                mrzLine2 = mrzLine2,
                navigateToSelfie = navigateToSelfie
            )
        }
    }

    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        Card(
            modifier = Modifier
                .size(150.dp)
                .testTag("circle"),
            shape = CircleShape,
            elevation = 2.dp
        ) {
            Image(
                painterResource(R.drawable.document),
                contentDescription = "",
                contentScale = ContentScale.Crop,
                modifier = Modifier.fillMaxSize()
            )
        }

        Text(
            modifier = Modifier.padding(vertical = 24.dp, horizontal = 16.dp),
            text = "Place smartphone on the passport",
            textAlign = TextAlign.Center,
            style = MaterialTheme.typography.h4
        )

        Divider()

        Text(
            modifier = Modifier.padding(vertical = 24.dp, horizontal = 16.dp),
            text = "Assuming that you have enabled NFC on your smartphone, place your smartphone on the identity document in order to scan and retrieve the identity data stored on the chip of the document. A sound will be trigger and once completed you will be forwarded to the next step.",
            style = MaterialTheme.typography.body1
        )
    }
}

fun Context.getActivity(): AppCompatActivity? = when (this) {
    is AppCompatActivity -> this
    is ContextWrapper -> baseContext.getActivity()
    else -> null
}

private fun nfcRead(context: Context) {
    val intent = Intent(context, context::class.java)
    intent.flags = Intent.FLAG_ACTIVITY_SINGLE_TOP
    val pendingIntent =
        PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
    val filter = arrayOf(arrayOf("android.nfc.tech.IsoDep"))
    val adapter = NfcAdapter.getDefaultAdapter(context)
    adapter.enableForegroundDispatch(context.getActivity(), pendingIntent, null, filter)
}

private fun onScan(
    context: Context,
    intent: Intent,
    mrzLine2: String,
    navigateToSelfie: (DocumentDetails) -> Unit
) {
    if (NfcAdapter.ACTION_TECH_DISCOVERED == intent.action) {
        val tag = intent.extras!!.getParcelable<Tag>(NfcAdapter.EXTRA_TAG)
        if (listOf(*tag!!.techList).contains("android.nfc.tech.IsoDep")) {
            val isoDep = IsoDep.get(tag)
            parseIsoDepData(
                context = context,
                mrzLine2 = mrzLine2,
                isoDep = isoDep,
                navigateToSelfie = navigateToSelfie
            )
        }
    }
}

private fun parseIsoDepData(
    context: Context,
    mrzLine2: String,
    isoDep: IsoDep,
    navigateToSelfie: (DocumentDetails) -> Unit,
) {
    val documentNumber = mrzLine2.substring(0..8)
    val birthday = mrzLine2.substring(13..18)
    val expiration = mrzLine2.substring(21..26)

    val bacKey = BACKey(documentNumber, birthday, expiration)

    val cardService: CardService = CardService.getInstance(isoDep)
    cardService.open()

    val service = PassportService(
        cardService,
        NORMAL_MAX_TRANCEIVE_LENGTH,
        DEFAULT_MAX_BLOCKSIZE,
        true,
        false
    )
    service.open()

    var paceSucceeded = false

    try {
        val cardSecurityFile =
            CardSecurityFile(service.getInputStream(PassportService.EF_CARD_SECURITY))
        val securityInfoCollection = cardSecurityFile.securityInfos
        for (securityInfo in securityInfoCollection) {
            if (securityInfo is PACEInfo) {
                service.doPACE(
                    bacKey,
                    securityInfo.objectIdentifier,
                    PACEInfo.toParameterSpec(securityInfo.parameterId),
                    null
                )
                paceSucceeded = true
            }
        }
    } catch (e: Exception) {
        Log.d("Vaios", "$e")
    }

    service.sendSelectApplet(paceSucceeded)

    if (!paceSucceeded) {
        try {
            service.getInputStream(PassportService.EF_COM).read()
        } catch (e: java.lang.Exception) {
            service.doBAC(bacKey)
        }
    }

    // -- Personal Details -- //
    val dg1In = service.getInputStream(PassportService.EF_DG1)
    val dg1File = DG1File(dg1In)

    // -- Face Image -- //
    val dg2In = service.getInputStream(PassportService.EF_DG2)
    val dg2File = DG2File(dg2In)

    var counter = 0
    val outputDirectory = FileSystem.outputDirectory
    dg2File.faceInfos.forEach {
        it.faceImageInfos.forEach {
            val file = File(outputDirectory, "passport_$counter.jpeg")
            saveImage(context, it, file)
            counter++
        }
    }

    val documentDetails = with(dg1File.mrzInfo) {
        DocumentDetails(
            documentNumber = documentNumber,
            surname = primaryIdentifier.replace("<", " ").trim { it <= ' ' },
            firstname = secondaryIdentifier.replace("<", " ").trim { it <= ' ' },
            birthday = convertFromMrzDate(dateOfBirth),
            issueDay = "",
            expiryDay = convertFromMrzDate(dateOfExpiry),
            birthPlace = "",
            authority = issuingState
        )
    }

    navigateToSelfie(documentDetails)
}

private fun convertFromMrzDate(mrzDate: String): String = try {
    val date = SimpleDateFormat("yyMMdd").parse(mrzDate)
    SimpleDateFormat("dd/MM/yyyy").format(date)
} catch (e: ParseException) {
    e.printStackTrace()
    ""
}

object NfcReaderDestination : NavigationDestination {

    const val documentTypeArg: String = "documentType"

    const val countryArg: String = "country"

    const val mrzLine2Arg: String = "mrzLine2"

    private const val baseRoute: String = "nfc_reader"

    override val route: String =
        "${baseRoute}/{${documentTypeArg}}/{${countryArg}}/{${mrzLine2Arg}}"

    fun routeForArguments(
        documentType: DocumentType,
        country: Country,
        mrzLine2: String,
    ): String = "${baseRoute}/${documentType}/${country}/${mrzLine2}"

    override val arguments: List<NamedNavArgument> = listOf(
        navArgument(documentTypeArg) {
            type = NavType.EnumType(DocumentType::class.java)
        },
        navArgument(countryArg) {
            type = NavType.EnumType(Country::class.java)
        },
        navArgument(mrzLine2Arg) {
            type = NavType.StringType
        },
    )
}