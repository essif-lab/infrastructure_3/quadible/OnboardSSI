plugins {
    id("com.android.application")
    kotlin("android")
    kotlin( "kapt")
}

android {
    compileSdk = libs.versions.compileSdk.get().toInt()
    defaultConfig {
        applicationId = "com.quadible.onboardssi"
        minSdk = libs.versions.minSdk.get().toInt()
        targetSdk = libs.versions.targetSdk.get().toInt()
        versionCode = 1
        versionName = "1.0"
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    packagingOptions {
        resources.excludes.add("META-INF/*.kotlin_module")
    }

    buildFeatures {
        // Enables Jetpack Compose for this module
        compose = true
        viewBinding = true
        dataBinding = true
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }

    compileOptions {
        isCoreLibraryDesugaringEnabled = true
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }

    composeOptions {
        kotlinCompilerExtensionVersion = libs.versions.composeCompiler.get()
    }
}

dependencies {
    implementation(project(":shared"))
    implementation(libs.bundles.app.ui)
    implementation(libs.bundles.camera)
    implementation(libs.bundles.mlkit)
    implementation(libs.bundles.ktor.common)
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.3.3")
    implementation(libs.scuba)
    implementation(libs.jmrtd)
    implementation(libs.jj2000)
    implementation(libs.jnbis)
    implementation(libs.coil.compose)
    implementation(libs.kotlinx.dateTime)
    coreLibraryDesugaring(libs.android.desugaring)
    implementation(libs.koin.android)
    testImplementation(libs.junit)
    kapt ("com.android.databinding:compiler:3.1.4")
    // VCX library
    implementation ("com.evernym:vcx:0.13.1-d69198c@aar")
    implementation ("net.java.dev.jna:jna:4.5.0@aar")

// optional, required to libVcx logger configuration
    implementation ("org.slf4j:slf4j-api:1.7.30")
    implementation("com.github.bright:slf4android:0.1.6"){
        this.isTransitive = true
    }

// optional, required to work with CompletableFuture implementation used by libVcx
    implementation ("net.sourceforge.streamsupport:android-retrofuture:1.7.1")

    implementation ("androidx.core:core-ktx:1.3.1")
    implementation ("androidx.fragment:fragment-ktx:1.2.5")
    implementation ("androidx.appcompat:appcompat:1.1.0")
    implementation ("androidx.constraintlayout:constraintlayout:1.1.3")
    implementation ("androidx.lifecycle:lifecycle-extensions:2.2.0")
    implementation ("androidx.lifecycle:lifecycle-viewmodel-ktx:2.2.0")
    implementation ("com.google.android.material:material:1.1.0")
    implementation ("androidx.room:room-runtime:2.4.3")
    implementation ("androidx.room:room-ktx:2.4.3")
    implementation ("com.journeyapps:zxing-android-embedded:4.1.0")
    implementation ("com.squareup.okhttp3:okhttp:4.8.1")
    implementation ("com.squareup.okhttp3:logging-interceptor:4.8.1")
    implementation ("com.github.bumptech.glide:glide:4.11.0")
    // could probably provide own log level implementation
    api("com.github.bright:slf4android:0.1.6"){
        this.isTransitive = true
    }
    implementation ("org.slf4j:slf4j-api:1.7.30")
    implementation ("net.java.dev.jna:jna:4.5.0@aar")

    implementation ("com.evernym:vcx:0.13.1-d69198c@aar")
    api ("net.sourceforge.streamsupport:android-retrofuture:1.7.1")
    api ("net.sourceforge.streamsupport:android-retrostreams:1.7.1")
    implementation ("de.adorsys.android:securestoragelibrary:1.1.0")
    implementation ("androidx.annotation:annotation:1.1.0")
    implementation ("androidx.annotation:annotation-experimental:1.0.0")
    implementation ("androidx.annotation:annotation-experimental-lint:1.0.0")
    implementation ("net.lingala.zip4j:zip4j:2.6.1")
    implementation ("com.squareup.okhttp3:okhttp:4.8.1")
    implementation ("com.squareup.okhttp3:logging-interceptor:4.8.1")

    implementation ("org.bitbucket.quadible:androidlib:0.1.46")

}